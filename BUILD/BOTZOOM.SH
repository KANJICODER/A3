    ## HKMF_A3_BUILD_BOSS ######################################
    gcc                                                        \
        -x c                                                   \
        -c "./SUB/BOTZOOM/KBZ.C11"                             \
        -o object_file.o                                       \
                                                               \
            -Wno-overlength-strings                            \
                                                               \
            -Wconversion                                       \
                                                               \
            -Werror                                            \
            -Wpedantic                                         \
            -Wall                                              \
            -Wextra                                            \
                                                               \
            -fstrict-aliasing                                  \
            -Wstrict-aliasing                                  \
                                                               \
            -Wfatal-errors                                     \
                                                               \
            -std=c11                                           \
            -m64 ###############################################
                                        ####                ####
    gcc -o BOTZOOM.exe object_file.o    ####                ####                
    rm                 object_file.o    ####                ####  
    mv   ./BOTZOOM.exe ./SANDBOX/BOTZOOM.exe
    cd                 ./SANDBOX
         ./BOTZOOM.exe  ## "UTM"        ####                ####
    rm   ./BOTZOOM.exe
    cd ..
                                        ####                ####
    ############################################################

##  Stop_At_First_Error  :  -Wfatal-errors                    ##
    
##  Strings Over 4096 OK :  -Wno-overlength-strings           ##

##  KBZ == Kyoot Bot Zoom , 3 letter version of BOTZOOM       ##