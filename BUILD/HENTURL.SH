    ## HKMF_A3_BUILD_BOSS ######################################
    gcc                                                        \
        -x c                                                   \
        -c "./SUB/HENTURL/HUR.C11"                             \
        -o object_file.o                                       \
                                                               \
            -Wno-overlength-strings                            \
                                                               \
            -Wconversion                                       \
                                                               \
            -Werror                                            \
            -Wpedantic                                         \
            -Wall                                              \
            -Wextra                                            \
                                                               \
            -fstrict-aliasing                                  \
            -Wstrict-aliasing                                  \
                                                               \
            -Wfatal-errors                                     \
                                                               \
            -std=c11                                           \
            -m64 ###############################################
                                        ####                ####
    gcc -o HENTURL.exe object_file.o    ####                ####                
    rm                 object_file.o    ####                ####  
    mv   ./HENTURL.exe ./SANDBOX/HENTURL.exe
    cd                 ./SANDBOX
         ./HENTURL.exe  ## "UTM"        ####                ####
    rm   ./HENTURL.exe
    cd ..
                                        ####                ####
    ############################################################

##  Stop_At_First_Error  :  -Wfatal-errors                    ##
    
##  Strings Over 4096 OK :  -Wno-overlength-strings           ##

##  KBZ == Kyoot Bot Zoom , 3 letter version of BOTZOOM       ##
##  MOS == MOSaic l       , 3 letter version of MOSALUT       ##
##  HAR == Hentai_ARt     , 3 letter version of HENTART       ##
##  HUR == Hentai_URl     , 3 letter version of HENTURL       ##