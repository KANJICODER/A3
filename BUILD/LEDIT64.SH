    ## HKMF_A3_BUILD_BOSS ######################################
    gcc                                                        \
        -x c                                                   \
        -c "./SUB/LEDIT64/LE6.C11"                             \
        -o object_file.o                                       \
                                                               \
            -Wconversion                                       \
                                                               \
            -Werror                                            \
            -Wpedantic                                         \
            -Wall                                              \
            -Wextra                                            \
                                                               \
            -fstrict-aliasing                                  \
            -Wstrict-aliasing                                  \
                                                               \
            -Wno-overlength-strings                            \
                                                               \
            -std=c11                                           \
            -m64 ###############################################
                                        ####                ####
    gcc -o LEDIT64.exe object_file.o    ####                ####                
    rm                 object_file.o    ####                ####  
    mv   ./LEDIT64.exe ./SANDBOX/LEDIT64.exe
    cd                 ./SANDBOX
         ./LEDIT64.exe                  ####                ####
    rm   ./LEDIT64.exe
    cd ..
                                        ####                ####
    ############################################################

##  Stop_At_First_Error :  -Wfatal-errors                     ##
    