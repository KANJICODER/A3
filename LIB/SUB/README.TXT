
Sub System Summaries:

    LEDIT64 : Level Editor 64

        Has it's own stand alone win32 and opengl code
        and is used to edit one level worth of data at a
        time. The level editor can only fixate on one
        level at a time when editing.

        Has it's own keyboard code as well.

        This exists so we can start paiting something that
        looks cool as soon as possible.