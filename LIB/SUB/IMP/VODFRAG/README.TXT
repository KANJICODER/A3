//:VODFRAG:README:===========================================://

    #_ABOUT_E_S_MACROS_#
        If this code is going to compile
        as GLSL code by eventually wrapping
        it into a big string, we cannot have
        any double quotes or single quotes
        in the code, as that will wreck our
        pre-processing.
        
    @SUBSYSTEM_CONTRACT@
        Means the code only exists to fulfil some 
        contractual obligation of the project structure.
        We auto-generate calls to certain sub-library
        functions like *_INI() and *_UTM() .
        Which kind of creates a rudimentary
        interface if you think about it.
        
//:===========================================:VODFRAG:README://
