//:FILE_MACROS:==============================================://
#define                   CHR  const char
#define                   U08  uint8_t  
#define                   U32 uint32_t  
#define                   I32  int32_t  
#define                   REI  struct  A3T_REI
#define NOT_A_POINTER_GRIDPIX  struct  A3T_HENTAIF_GRIDPIX
#define     NOT_A_POINTER_REI  struct  A3T_REI
//:==============================================:FILE_MACROS://
//:ABOUT_TYPES_IN_THIS_FILE:=================================://
/** ******************************************************** ***

    A3T_HENTAIF_GRIDPIX :

        Helps us define a subselection(overlay)
        of an image, and a tile grid imposed
        over that subsection(overlay).

        Goal: #_DECOUPLE_STAMPING_FROM_FILTERING_#

    A3T_HENTAIF_FAMSTAM :

        Groups a SOURCE and DESTINATION gridpix
        together with a filter function handle.
        Using a filter function handle instead
        of a function pointer to make code 
        more readable. I give zero fucks that
        a big [ case | switch ] to call function
        based on enumeration value is slower.

        We need code that I can debug by READING it.
        Passing around function pointers can be
        great for making things flexible. But
        unless there is no other way, avoid it.

        It always makes the code hard to debug
        and even when it works, hard to follow.

        Goal: #_DECOUPLE_STAMPING_FROM_FILTERING_#


*** ******************************************************** **/
//:=================================:ABOUT_TYPES_IN_THIS_FILE://

    /** **************************************************** ***

    //:------------------------------------------://
    //: GRIDPIX                                  ://            
    //:     +----------------------------------+ ://
    //:     | objlock                          | ://
    //:     +----+-----------+-----------+-----+ ://
    //:     |p_b | p_w , p_h |           |     | ://
    //:     |o_r | o_w , o_h | o_x , o_y |     | ://
    //:     |    | g_w , g_h |           | g_n | ://
    //:     |    | t_w , t_h |           | t_n | ://
    //:     +----+-----------+-----------+-----+ ://
    //:------------------------------------------://

    *** **************************************************** **/

    struct A3T_HENTAIF_GRIDPIX{ //:PUBLIC_STRUCT://
        //:  1234567
        I32  objlock ; //:Object Lock Flag 
                       //:
                       //: We load this object with
                       //: [independent]property values
                       //: and then use[ GRIDPIX_OBJLOCK ]
                       //: to calculate derived properties.
                       //: If we wanted to re-use this
                       //: object we could call a
                       //: [ GRIDPIX_OBJOPEN ], edit
                       //: [independent]properties and
                       //: then re-lock it.

        //:INDEPENDENT_PROPERTIES:---------------------------://

        U08* p_b ; //:Pixels:BUF://
        I32  p_f ; //:Pixels:F_R_E_E_able ? ://
        I32  p_w ; //:Pixels:WID://
        I32  p_h ; //:Pixels:HIG://
        
        REI  o_r ; //:Overlay Rectangle Where Grid Exists://
                   //:#_WHY_NAMED_O_R_#://
                      
        I32  g_w ; //:Grid Overlay Width
        I32  g_h ; //:Grid Overlay Height
        
        //:---------------------------:INDEPENDENT_PROPERTIES://
        //:DERIVED_PROPERTIES:-------------------------------://
        
        I32  g_n ; //:Number Of Grid Cells( g_w * g_h )
        
        I32  o_x ; //:Overlay's X Origin (origin/offset)
        I32  o_y ; //:Overlay's Y Origin (origin/offset)
        I32  o_w ; //:Overlay's WIDTH
        I32  o_h ; //:Overlay's HEIGHT
        
        I32  t_n ; //:Number Of Pixels In One Tile(t_w*t_h)
        I32  t_w ; //:Width In Pixels Of One Tile In Grid
        I32  t_h ; //:Width In Pixels Of One Tile In Grid

        //:-------------------------------:DERIVED_PROPERTIES://

    };
    
    struct A3T_HENTAIF_FAMSTAM{
    
        //:Function Index, to avoid callbacks to make://
        //:the code more readable.
        
            I32 fec ; //: @fec@ : function_enumeration_code ://

        //:When done stamping, we only need to save
        //:[ hen_256 ]'s pixels. But when debugging,
        //:we might want to save the input image as well.

            CHR* hen_256_txt ; //:Where To Save[ hen_256 ]://
            CHR* pak_512_txt ; //:Where To Save[ pak_512 ]://
    
        //:Iterator Index That Controls Moving The
        //:64x64 pixel selector rectangles over
        //:their respective[ GRIDPIX ]objects.
        //:Pixel selector rectangles do[ NOT ]go
        //:inside[ GRIDPIX ]class because I want
        //:the[ GRIDPIX ]to be immutable data
        //:once object is sealed.
        //:sealed: ( GRIDPIX.objlock >=1 )
        //:
        //:   START[ dex==(0-1) , kog==( 1 ) ]
        //:     END[ dex==(0-1) , kog==(0-1) ]
        //: INVALID[ dex==( 0 ) , kog==( 0 ) ]
        //:
        //:To re-start iterator after end, all you need
        //:to do is flip[ kog ]back to 1.
        //:
        //:Choose these values because of way I do things
        //:and decided I didn't want all zero to start out
        //:because the object can be zero initialized
        //:into a valid state.
        
            I32 dex ; //: Valid if 0 or positive.
            
            I32 kog ; //: Keep On Going?  
                      //: dex is valid if kog is >=1

            I32 f16 ; //:#_F16_DERIVED_FROM_DEX_IN_TIK_#://
            I32 fm4 ; //:#_FM4_DERIVED_FROM_DEX_IN_TIK_#://
    
        //:Hentai Source Image Grid Pixels 
        //:Current Use Case Is[ 256x256 ]Pixels 
        //:The[ 256 ]represents a sub-selection size.
        //:[ hen_256 ]could be a[ 256x256 ]sub
        //:selection of a giant[ 666K_x_666K ]image
        
            NOT_A_POINTER_GRIDPIX 

            hen_256 ;  //:SOURCE://
    
        //: The sub selection where we are pasting
        //: [Auset]graphics into an [Aupak].
        //: The[ 512x512 ]describes the[ Aupak ]'s
        //: worth of pixels. (64x64 pixels per Auset).
        
            NOT_A_POINTER_GRIDPIX 

            pak_512 ;  //:DESTINATION://

        //: Goal of each tik of[ A3F_HENTAIF_FAMSTAM_TIK ]://
        //: Is to calculate the next pair of "cut"+"paste"://
        //: selection rectangles.( [ hen ]&[ pak ] )      ://

            NOT_A_POINTER_REI  hen ; //:Hentai_SRC__Pixels://
            NOT_A_POINTER_REI  pak ; //:Destination_Pixels://
    };

//:TYPES_CREATED_FOR_CHAD_GAME:==============================://
/** ******************************************************** ***
    CTRL_C_HELP:

    ASS( ((PNGPAIR*)0)!=pngpair );

    (*pngpair).ori_png =( xxx_xxx );
    (*pngpair).ori_nob =( xxx_xxx );

    (*pngpair).aup_png =( xxx_xxx );
    (*pngpair).aup_nob =( xxx_xxx );

    (*pngpair).dec_wid =( xxx_xxx );
    (*pngpair).dec_hig =( xxx_xxx );

*** ******************************************************** **/

    //:ORIGINAL_PNGPAIR_PRE_2022_03_27:------------------://

        #define A3T_HENTAIF_PNGPAIR [FIX:*_CHADGAM_PNGPAIR]
        #define A3F_HENTAIF_PNGPAIR [DOUBLY_WRONG_MYFRIEND]

        struct A3T_HENTAIF_CHADGAM_PNGPAIR{
        //:....123_1234567_1234567_1234567://

            //: RESIZED: Original  Image, Maybe Tinted   ://
                U08* ori_png ;  //: @ori_png@ ://
                I32  ori_nob ;  //: @ori_nob@ ://
            
            //: RESIZED: Scrambled Version Of[ ori_png ] ://
            //:                           NOT[ png_bin ] ://
                U08* aup_png ;  //: @aup_png@ ://
                I32  aup_nob ;  //: @aup_nob@ :// 

            //: Decoded[WID]&[HIG]. When uncompressed    ://
            //: both images are exact same dimensions    ://
                I32  dec_wid ;  //: @dec_wid@ ://
                I32  dec_hig ;  //: @dec_hig@ ://

        };

    //:------------------:ORIGINAL_PNGPAIR_PRE_2022_03_27://
    //:PNGPAIR_EXTRA_2022_03_27:-------------------------://

    struct A3T_HENTAIF_CHADGAM_PPEXTRA{

        //: Decided on two distinct tints because        ://
        //: I might want to up the contrast on           ://
        //: one of these to make it stand out better.    ://

        U32 pngtint_ori ;  //: Tint To Apply To Original ://
        U32 pngtint_aup ;  //: Tint To Apply To Ausetpak ://

        //: This isn't going to be a... graphics perfect ://
        //: implementation of lumonosity adjustment.     ://
        //: Just a rough "make the image brigher or"     ://
        //: darker.                                      ://

        U08 pnglumo_ori ;  //: pnglumo 128 == nothing    ://
        U08 pnglumo_aup ;  //: pnglumo 255 == max bright ://
                           //: pnglumo   0 == max   dark ://

    };
    //:-------------------------:PNGPAIR_EXTRA_2022_03_27://

//:==============================:TYPES_CREATED_FOR_CHAD_GAME://
//:FILE_MACROS:==============================================://
#undef                    CHR
#undef                    U08  
#undef                    U32
#undef                    I32  
#undef                    REI  
#undef  NOT_A_POINTER_GRIDPIX
#undef      NOT_A_POINTER_REI
//:==============================================:FILE_MACROS://
/** ******************************************************** ***
*** COMMENTS_ARE_READ_LAST_OR_NEVER ************************ ***

    #_DECOUPLE_STAMPING_FROM_FILTERING_# :

        We want to ISOLATE the physical action of
        making our [robot/algorithm] move from
        one cell of source grid to one cell of the
        destination grid.

        ISOLATE this physical action from the pixel
        [ filtering / tile processing ]methods that
        are used once we have physically selected
        source and destination tile cells.

        REASONS:
            1.  I found it super difficult to add another
                tile graphic processing pipeline to this
                code because a lot of the same code
                was needing to be re-written.
                
                Which meant, time for some re-usable code.

            2.  Though  my initial algorithms work to
                create acid_trip_hentai_tiles from
                hentai_porn_source_image , the code
                is really fucking hard to follow.
                
                And this  next pixel processing algorithm
                is anticipated to be even harder
                than the first one.

    #_WHY_NAMED_O_R_# 
        [QUESTION]:Is there a reason I didn't   
        name it[ g_o ]For[Grid_Overlay]?
        [ o_r ]seems intentionally detached
        Virusally from[ p_b,p_f,p_w,p_h ]
        AND[ g_w,g_h,g_n ]

    #_F16_DERIVED_FROM_DEX_IN_TIK_# 
        <FAMSTAM>.f16 is derived from <FAMSTAM>.dex
         VIA: A3F_HENTAIF_FAMSTAM_TIK( <FAMSTAM> )

        @f16@ : The family the Auset tile belongs to.
                There are 16 families in the game.
                Meaning we can have 16 total unique
                materials within the game.
                ( Each material having 4 visual variations )
                ( because of[ fm4 ]                        )

    #_FM4_DERIVED_FROM_DEX_IN_TIK_# 
        <FAMSTAM>.fm4 is derived from <FAMSTAM>.dex
        VIA: A3F_HENTAIF_FAMSTAM_TIK( <FAMSTAM> )

        @fm4@ : Family Member (4 of them)[0|1|2|3]
                Basically is a [ varation version number ]
                of a material type.
                ( auset == autotileset == material type )

    @ori_png@ : Original PNG Binary File (resized to[ dec ])
    @ori_nob@ : Original PNG binary File, number of bytes
    
    @aup_png@ : AusetPack(aup) PNG Binary (resized to[ dec ])
    @aup_nob@ : AusetPack(aup) PNG Binary, number of bytes
    
    @dec_wid@ : Decoded Width  ( because file __NOT__ pixels )
    @dec_hig@ : Decoded Height ( because file __NOT__ pixels )


*** ************************ COMMENTS_ARE_READ_LAST_OR_NEVER ***
*** ******************************************************** **/