//:DIAGRAMS:=================================================://
    |<-- 2*3*4 * 64  ------->|
    |<---- 1,536 Tiles ----->|
    |<-------- GU5 --------->|     |<-------- GU4 -------->|
        +---+        +---+                  _ _ _ _
        |   |        |   |                 |_|_|_|_|
    +---+---+---++---+---+---+             |_|_|_|_|
    |   |   |   ||   |   |   |             |_|_|_|_|
    +---+---+---++---+---+---+      _ _ _ _|_|_|_|_|_ _ _ _
        |   |        |   |         |_|_|_|_| . . . |_|_|_|_|
        +---+        +---+         |_|_|_|_| . . . |_|_|_|_|
        +---+        +---+         |_|_|_|_| . . . |_|_|_|_|
        |   |        |   |         |_|_|_|_|_._._._|_|_|_|_|
    +---+---+---++---+---+---+             |_|_|_|_|
    |   |   |   ||   |   |   |             |_|_|_|_|
    +---+---+---++---+---+---+             |_|_|_|_|
        |   |        |   |                 |_|_|_|_|      GU1
        +---+        +---+                                 |
       /     \                 |<------ GU2 ------>|       v
      ._ _ _ _.--------------->+---+---+...+---+---+ --> +---+
      |_|_|_|_|                |   |   |   |   |   |     |1X1|
      |_|_|_|_|                +---+---+   +---+---+ --> +---+
      |_|_|_|_|                |   |           |   |       ^
      |_|_|_|_|                +---+           +---+       |
      |<-4X4->|\                ...    64x64    ...       GU1
      |<-GU3->| \              +---+           +---+
                 \             |   |           |   |
                  \            +---+---+   +---+---+
                   \           |   |   |   |   |   |
                    +--------->+---+---+...+---+---+
//:=================================================:DIAGRAMS://
//:WHY_I_CHOOSE_THE_NUMBERS_I_DID:===========================://

    How Many Units Should Be Inside 1 alice sized tile?

    Enough to get us addressing space for all the different
    nesting levels of our fractal auto tile engine
    without having to create a math library for weird
    compound multi-component numbers.
    (I've tried making my own BigNumber style numbers)
    (before and it is a nightmare.)
    
    4,294,967,295 / 1,536 == 2,796,202
    4294967295 / 1536 == 2796202
    
    How about 16 bits of addressing space
    per tile? 65,535 (unsigned)
    
    65536 / 2 == 32,768 : Nesting Level 0
    32768 / 2 == 16,384 : Nesting Level 1
    8,192 / 2 ==  4,096 : Nesting Level 2
    4,096 / 2 ==  2,048 : Nesting Level 3

    Should be enough space to address all of
    the different nesting levels we want for
    our fractal auto tiles.

//:===========================:WHY_I_CHOOSE_THE_NUMBERS_I_DID://