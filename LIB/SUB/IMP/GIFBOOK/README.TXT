//:THIS_CODE_IS_NOT_FINISHED:!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!://

    The idea was to create a fun little animated gif maker.
    But the problem is, even if we were to pull off the
    re-entrant loop code and get this working, the gifs would
    be of poor quality.
    
    This system will be commented out of code base.
    Start working on world map and [level-pack / room-pack]
    generation code. DATE[ 2021_07_20 ]

//:!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!:THIS_CODE_IS_NOT_FINISHED://
//:GIFBOOK_SUMMARY:==========================================://

    A single threaded asynchronous batch processor that
    takes all the .PNG files within a folder and turns 
    them __ALL__ into animated gifs.
    
    YES. SINGLE THREADED
    YES. ASYNCHRONOUS
    
    Every call of A3F_GIFBOOK_TIK() moves the processing
    forward by 1 frame of animation processing for the
    current .PNG file being turned into an animation.

//:==========================================:GIFBOOK_SUMMARY://
//:DATA:=====================================================://

    a3d_gifbook_act : (ACTive instance)
    
        If the system is running, this is a reference
        to the[ A3T_GIFBOOK ]that was used to launch
        the work. This variable is needed so we can
        query A3F_GIFBOOK_DKMIWTL() without giving it
        a[ A3T_GIFBOOK ]instance.
        
        THIS VARIABLE IS READ ONLY UNTIL PASSED TO DEEPER
        FUNCTION IN THE SYSTEM.
        
    a3d_gifbook_ron_g_b : ReadONly_GifBook
  
        Old name for[ a3d_gifbook_act ]
        
    lis_arr : LIST the actual ARRay of whatever.
    lis_len : LIST the number of elements in it (len)
    lis_i32 : LIST , an array of uint32_t (I32)

//:FUNCTIONS:================================================://
    
    A3F_GIFBOOK_INTLIST_MAK : Copy   NULL_TERMED List Of Ints
    A3F_GIFBOOK_INTLIST_DEL : DELETE NULL_TERMED List Of Ints

    A3F_GIFBOOK_ZOS : Zero_Out_Struct (A reset if you will)

    A3F_GIFBOOK_KOT : Keep_On_Ticking
        Return 1 if the system wants to stay alive.
        Only when ALL SYSTEMS in the game loop all report
        back[ 0 ]do we exit the main loop and terminate
        the [ game / process ].
    
        Function Name Contenders:
        KOT : Keep On Ticking ( Works With TIK() verbage )
        KMA : Keep Me Alive
        KIA : Keep It Alive
        KAL : Keep ALive
        DKM : Dont Kill Me
        WTL : Want To Live

    A3F_GIFBOOK_TIK :
        If work was previously issued by A3F_GIFBOOK_RUN ,
        this will move the work forward by the smallest
        indivisible unit of work possible.
        (So we don't stall our window loop when running)
        (everything on the same thread.                )
        
    A3F_GIFBOOK_RUN_DEF : Run Default.
        Will look for a few default folders, and will choose
        the first one found with images in it to run it's
        processing logic on.
        
    A3F_GIFBOOK_RUN :
        Will attempt to run gif batch processing on the folder
        specified. Will fail without crashing if:
        
        1: The processor is already running.
        2: The folder specified does not exist.
        
        To avoid confusion as to where we left off:
        1: We REST on index to be [processed/processing]
        2: [ 0 == pha ]==> TO_BE_PROCESSED.....
        3: [ 1 == pha ]==> BEING_PROCESSED.....
        4: [ 2 == pha ]==> DONE__PROCESSING!!!!
        
        ( 2 == pha ) should never be reached!
        Because we move onto the next file via:
        
        lip_dex ++
        lip_pha=( 0 )
        
        When we are done processing the previous.
        
    A3F_GIFBOOK_INI : 
        Initialize data for subsystem if needed.
    
//:================================================:FUNCTIONS://
//:TYPES:====================================================://
        
    A3T_GIFBOOK :
        A singleton (for now) struct that stores state 
        information for the GIFBOOK system.
        
        I decided on a singleton for the reason that it
        makes it easier to make the code terser because:
        
            a3d_gifbook_lip_dex
            a3d_gifbook_lip_len
            a3d_gifbook_lip_arr
            a3d_gifbook_lip_pha
            
            a3d_gifbook_ani_dex
            a3d_gifbook_ani_len
            a3d_gifbook_ani_pha
            
        Is much more unweildy with column space than:
        
            #define G_B A3D_GIFBOOK
            
                G_B.lip_dex
                G_B.lip_len
                G_B.lip_arr
                G_B.lip_pha
                
                G_B.ani_dex
                G_B.ani_len
                G_B.ani_pha
                
            #endif
            
        And the tokens are still unique enough that we won't
        confuse them with variables in another system.
        
//:====================================================:TYPES://
//:HASHTAG_COMMENTARY:=======================================://

    #_RON_G_B_IS_NEVER_NULL_#
    
        Make sure readonly[ G_B* ]is never null by
        initializing to point to the default singleton
        instance. Makes other code less complex when I
        dont have to check for null everywhere.
        
    #_WAS_STAP_LAUNCHED_# :
    
        WAS Single_Threaded_Async_Process LAUNCHED ?
        The worker is single threaded and async.
        Kind of like how JavaScript works.
        
    #_LIP_PHA_#

        To avoid confusion with a given image index ,       
        these flags will tell use what we are currently     
        doing with the image pointed to by[ lip_dex ]       

    #_ANI_PHA_#
    
        To avoid confusing with a given image frame (ani_dex) ,
        these flags tell us what state of processing we are
        in for a given frame of an animated gif.
        
    #_FIL_PHA_# 

        The next filter to apply on the given frame.
        For each frame described by #_ANI_PHA_# , we
        have multiple filters we will want to layer on.

    #_FILTER_PHASE_BREAKS_UP_ANIMATION_FRAMES_NOT_PIXELS_#
    
        Individual filters that are part of animation are     
        not broken into pausable steps. That would be         
        overkill as it would drastically change how you       
        would have to write image filters. And you'd have     
        to constanly think of unique ways to pause execution. 
        
    #_OUTER_NEX_FUNCTION_IS_A_RE_ENTRANT_LOOP_#
    
        Looking at a3f_gifbook_nex_all , it might not be
        obvious that each function call is basically a
        different nesting level of a re-entrant loop.
        Now you know.
        
    #_PHASE_IDLE_SPIN_# : 
        Means that work was never issued or that work 
        is currently done. Because of our re-entrant 
        "fake nested" loop structure, [ nex_cfg ] function
        _WILL_ be called when it has no work to do in
        multiple scenarios.
        
    #_DEEPER_WORKER_DECIDES_WHEN_YOU_LEAVE_HERE_#
        //:------------------------------------------://
        //: Do nothing. You are stuck in this phase  ://    
        //: Until a deeper re-entrant loop function  ://
        //: decides we are done here.                ://
        //:------------------------------------------://
        //:--------------------------------------://
        //: MIDDLE PHASE ALWAYS WAITS IDLE UNTIL ://
        //: CONCEPTUALLY DEEPER PHASE MOVES THE  ://
        //: MIDDLE PHASE COUNTER FORWARD:        ://
        //:--------------------------------------://
        
    #_CLEANUP_CODE_PHASE_#
    
        Cleanup code goes into this section _IF_
        there is some type of cleanup that needs
        to be done.
        
//:=======================================:HASHTAG_COMMENTARY://
//:ABBREVIATIONS:============================================://

    NULL_TERMED : Null Terminated.
    
//:============================================:ABBREVIATIONS://