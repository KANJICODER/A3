//:GIFBOOK_NEX:Re_Entrant_Loop_Code:-------------------------://

    #_MAK_OR_RES_WORKS_FOR_THIS_CFG_BLOCK_#
        Could be put in RES_001 instead and should      
        still have same behavior since these settings   
        not needed to be cleaned up.                    
        
    @_SWIHACK_@ : SWItch statement HACK

    #_RFF_ELD_# : (Reset_Function_For : Each_Loop_Depth)
        Call Reset Function For Each Loop Depth( y )    
        Reset Functions Always Stored In X==2 slot.     

    ------------------------------------------------------------
    The work on this has been a disorganized disaster.
    Trying to keep the comments away from the code even
    though I am doing a lot of unique things I might be
    like "WHAT THE HELL WAS I THINKING!?" a few months
    from now and have no comments directly in the code
    to help me.
    
    I am taking a risk here though. Scared of writing code
    without the comments in it, but we need to observe which
    styles of code are more maintainable. This code
    base style is going for spares comments in code, but
    verbose documentation that is linked to in
    #_TAGGED_COMMENTS_LIKE_THIS_#
    ------------------------------------------------------------
    
    #_MAK_FAIL_OR_COR_FINISH_#
        The MAK() command has failed to allocate or
        the COR() function says it has no more loop
        iterations left. The MAK() function failure is
        designed to be recoverable (since malloc & calloc )
        ( are allowed to fail according to C spec ).
        Failure of MAK() will cause jump --> RES() --> DEL()
        Finish  of COR() will cause jump --> RES() --> DEL()
        
        ( Reset (RES) always jumps to Delete (DEL) )
        
        We indirectly jump to delete code so the loop
        counters can be zeroed out in RES() before unwinding
        to the previous scope of nested loop structure.
        
        //:--------------------------------------------------://
        //:#_NEX_ALL_NOTES_ARE_A_DISASTER_#                  ://
        //:#_THIS_CODE_IS_A_LOOP_#                           ://
        //:--------------------------------------------------://
        //: Conceptual Nested Flow Of Loops                  ://
        //:--------------------------------------------------://
        //: NEX_RUN( g_b ); <-- Advance The OuterMost Phase  ://
        //: NEX_CFG( g_b ); <-- Load Configuration           ://
        //: NEX_BUF( g_b ); <-- Load Or Assert Next Buffer   ://
        //: NEX_LIP( g_b ); <-- Load Next Image From Disk    ://
        //: NEX_ANI( g_b ); <-- Load Next Animation Frame    ://
        //: NEX_FIL( g_b ); <-- Use  All Filters On Frame    ://
        //:--------------------------------------------------://
        //:NOTES:FROM:RE4.C11 ( RE_ENTRANT_NESTED_LOOP_004 ) ://
        //:                                                  ://
        //: o_k == ( -1 ) : Loop Level Done, Or Error        ://
        //: o_k == ( +1 ) : Next Loop Iteration Or Go Deeper ://
        //:                                                  ://
        //: ( -1 ) == MAK_000() : Error,   allocation failed ://
        //: ( -1 ) == DEL_000() : Error,         free failed ://
        //: ( -1 ) == COR_000() : THIS FUNCTION DONE LOOPING ://
        //: ( +1 ) == COR_000() : Function  Has  More  Loops ://
        //: ( +1 ) == DEL_000() : Success,       free worked ://
        //: ( +1 ) == MAK_000() : Success, allocation worked ://
        //:                                                  ://
        //: ( +1 ) == RES_000() : Success, reset func pass   ://
        //: ( -1 ) == RES_000() : RESET FUNCTION NOT ALLOWED ://
        //:                       TO FAIL!!!!!!!!!!!!!!!!!!!!://
        //:--------------------------------------------------://
        
        
        @kot@ : kot == Keep_On_Ticking
        @_INC_B4U_INI_NEG_ONE_@ : Incremented Before Used
                                  Initialize It To NEGative One
        
//:-------------------------:GIFBOOK_NEX:Re_Entrant_Loop_Code://