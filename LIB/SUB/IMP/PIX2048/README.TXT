    
    MASTER DIAGRAMS FILE FOR PIX2048 IS[ PIX._ ]
    
    +---+---+---+---+ @TAU@   : Texture_As_Uniform
    |TAU|WOR| FIIRE | @WOR@   : 512x512 level pack world map
    +---+---+ SOUND + @FIIRE@ : Animated Fire  Blast Tiles
    |   |   | LASER | @SOUND@ : Animated Sound Blast Tiles
    +---+---+---+---+ @LASER@ : Animated Laser Beam  Tiles
    |   |   |LEP|LEP| <-- @LEP@ : LEvel Pack.
    +---+---+---+---+
    |   |   |SAB|SAB| <-- @SAB@ : Sprites_And_Bombs
    +---+---+---+---+
              ^   ^       +---+
              |   |       |   | <-- Empty, Unused at this time.
             [0] [1]      +---+