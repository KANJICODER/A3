//:BASE512:ABOUT:============================================://
 
    BASE512 ( aka: BASEIMG / Base Images )
    Use the watermaker to generate some default
    images that you can apply the animated filters
    to. 
    
    Basically, spawn a folder with these default images
    so that your gif batch processor can then
    read all the files and do it's thing.
    
    GIF batch processor == GIFBOOK
    
    All images are exactly 512x512 pixels for simplicity.
    Hence the name "base512".
    
//:============================================:BASE512:ABOUT://