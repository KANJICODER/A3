//:TETCAKE_FUNCTIONS:========================================://

    VOD a3f_shell64_tetcake_fm4( 
        I32 fm4 //:FamilyMemberindex[0-to-3 ]
    ){
    /** **************************************************** ***
    *                                                          *
    *   DIAGRAM[ #_WHAT_KIND_OF_NUT_# ]                        *
    *                                                          *
    *   100% method: Creates TOP     output graphic            *
    *    75% method: Creates LEF+RIG output graphic(s)         *
    *    25% method: Creates BOT     output graphic(s)         *
    *                                                          *
    *    The pixel[ GRABBING ]locations change depending on    *
    *    value of[ fm4 ]. The pixel[ PUTTING ]locations        *
    *    __ALWAYS_STAY_THE_SAME__.                             *
    *                                                          *
    *   if[ fm4 == 0 ]: (0_DEG_ROT)                            *
    *                                                          *
    *  [][][]|[][][] [][][] [][][] [][][] [][][] [][][] [][][] *
    *  []    |    [] []\       /[] []\       /[] []         [] *
    *  []    |    [] []  \   /  [] []  \   /  [] []         [] *
    *  []    o    [] []    o    [] []    o    [] []    o    [] *
    *  []   100%  [] []   75%   [] []   75%   [] []  /25%\  [] *
    *  []         [] []         [] []         [] []/       \[] *
    *  [][][]|[][][] [][][]|[][][] [][][]|[][][] [][][]|[][][] *
    *                                                          *
    *   if[ fm4 == 1 ]: (90_DEG_CLOCKWISE)                     *
    *                                                          *
    *  [][][]|[][][] [][][]|[][][] [][][]|[][][] [][][]|[][][] *
    *  []         [] []        /[] []        /[] []\        [] *
    *  []         [] []      /  [] []      /  [] []  \      [] *
    *  []    o------ []    o    [] []    o    [] []25% o    [] *
    *  []   100%  [] []   75%\  [] []   75%\  [] []  /      [] *
    *  []         [] []        \[] []        \[] []/        [] *
    *  [][][]|[][][] [][][]|[][][] [][][]|[][][] [][][]|[][][] *
    *                                                          *
    *                                                          *
    *   if[ fm4 == 2 ]: (180_DEG_CLOCKWISE)                    *
    *                                                          *
    *  [][][]|[][][] [][][]|[][][] [][][]|[][][] [][][]|[][][] *
    *  []         [] []         [] []         [] []\       /[] *
    *  []   100%  [] []   75%   [] []   75%   [] []  \25%/  [] *
    *  []    o    [] []    o    [] []    o    [] []    o    [] *
    *  []    |    [] []  /   \  [] []  /   \  [] []         [] *
    *  []    |    [] []/       \[] []/       \[] []         [] *
    *  [][][]|[][][] [][][]|[][][] [][][]|[][][] [][][]|[][][] *
    *                                                          *
    *                                                          *
    *   if[ fm4 == 3 ]: (270_DEG_CLOCKWISE)                    *
    *                                                          *
    *  [][][]|[][][] [][][]|[][][] [][][]|[][][] [][][]|[][][] *
    *  []         [] []\        [] []\        [] []        /[] *
    *  []         [] []  \      [] []  \      [] []      /  [] *
    *  ------o    [] []    o    [] []    o    [] []    o 25%[] *
    *  []   100%  [] []  /75%   [] []  /75%   [] []      \  [] *
    *  []         [] []/        [] []/        [] []        \[] *
    *  [][][]|[][][] [][][]|[][][] [][][]|[][][] [][][]|[][][] *
    *                                                          *
    *** **************************************************** **/
    #define B_0 ( PLD.b_2[0].b_4.buf )   //:BAMSHEL#1   ://
    #define B_1 ( PLD.b_2[1].b_4.buf )   //:BAMSHEL#2   ://
    #define TET ( PLD.t_3.buf        )   //:TETSHEL     ://
    #define CAK  tetcake                 //:TETCAKE/EDGE://
    #define NOP ( PLD.lon )              //:Num_Pixels  ://

    assert( PLD.san            == PLD.lon );
    assert( PLD.i_1.wid        == PLD.san );
    assert( PLD.i_1.hig        == PLD.san );
    assert( PLD.b_2[0].b_4.nop == PLD.lon );
    assert( PLD.b_2[1].b_4.nop == PLD.lon );
    assert( PLD.t_3.nop        == PLD.lon );
    assert( PLD.e_4[0].wid     >=    1    );
    assert( PLD.e_4[0].hig     >=    1    );
    assert( PLD.e_4[1].wid     >=    1    );
    assert( PLD.e_4[1].hig     >=    1    );
    assert( PLD.e_4[2].wid     >=    1    );
    assert( PLD.e_4[2].hig     >=    1    );
    assert( PLD.e_4[3].wid     >=    1    );
    assert( PLD.e_4[3].hig     >=    1    );
    assert( PLD.o_5.wid        == PLD.san );
    assert( PLD.o_5.hig        == PLD.san );

        I32  p16 ; //: @p16@ : Pair Of[ s32 ]values. 16pairs.://
        ///  s32 ; //:ShellLayerIndex[ 0-to-64 ]
        I32  se4 ; //:Side_Enumeration[ 0-to-3 ]
        I32  dfe ; //: @dfe@ : Distance_From_Edge ://
             
        I32  s32_000 ; //: [ s32 ]first taken.
        I32  s32_001 ; //: [ s32 ]2nd   taken.
        
        U08* tetcake ; //: @tetcake@ : Current[TETCAKE]buffer://

        I32      r_w ; //: WID IN PIXELS OF CUR[ tetcake ]   ://
        I32      r_h ; //: HIG IN PIXELS OF CUR[ tetcake ]   ://

        //:----------------------------------------------://
        //: One full[ p16 ]loop creates all layers       ://
        //: of our[ TETCAKE ]edge. Then[ se4 ]switches   ://
        //: which[  TETCAKE ]edge we are building.       ://
        //:----------------------------------------------://
        for( se4 = 0 ; se4 <= ( 4-1) ; se4 ++ ){
        for( p16 = 0 ; p16 <= (16-1) ; p16 ++ ){ 

            //:------------------------------------------://
            //: Odd And Even[ ONESHEL ]layers.           ://
            //: BAMSHEL is made from mirroring[ ONESHEL ]://
            //: onto itself. So you could call these     ://
            //: [ BAMSHEL ]layers if you wanteed to.     ://
            //:------------------------------------------://
            s32_000 = ( p16 * 2 ) + 0 ;
            s32_001 = ( p16 * 2 ) + 1 ;

            //:CREATE_TWO_BAMSHEL(S):====================://

                //:--------------------------------------://
                //:Create_1ST___BAMSHEL:
                a3f_shell64_bamshel_fm4_se4_s32(
                                    fm4,se4,s32_000 );;
                a3f_shell64_ass_bam(B_0,NOP,"[BAM_NOP]");
                //:--------------------------------------://
                //:Create_2ND___BAMSHEL:
                a3f_shell64_bamshel_fm4_se4_s32(
                                    fm4,se4,s32_001 );;
                a3f_shell64_ass_bam(B_1,NOP,"[BAM_NOP]");
                //:--------------------------------------://
                a3f_shell64_ass_bam(B_0,NOP,"[B_0:2_X]");
                a3f_shell64_ass_bam(B_1,NOP,"[B_1:2_X]");
                //:--------------------------------------://

            //:====================:CREATE_TWO_BAMSHEL(S)://
            //:MERGE_BAMSHELS_INTO_TETSHEL:==============://

                //:Merge By Taking Darkest Of Both Pixels.
                //:(Roughly). Reason: Because edges should
                //:be darker if we want them to read as
                //:edges.
                a3f_shell64_bleline_pix_pix_nop___pix(
                                    B_0,B_1,NOP , TET );;

                a3f_shell64_ass_bam(B_0,NOP,"[B_0:3_X]");
                a3f_shell64_ass_bam(B_1,NOP,"[B_1:3_X]");
                a3f_shell64_ass_tet(TET,NOP,"[TET_NOP]");

            //:==============:MERGE_BAMSHELS_INTO_TETSHEL://
            //:GET_TETCAKE_EDGE_BUFFER:==================://

                //: TETSHEL_IS_LAYER_OF_TETCAKE ://
                 
                CAK = &( PLD.e_4[ se4 ].pix[ 0 ] );
                r_w =  ( PLD.e_4[ se4 ].wid      );
                r_h =  ( PLD.e_4[ se4 ].hig      );

                if( SE4_TOP == se4 
                ||  SE4_BOT == se4  
                ){
                    assert( r_w = PLD.lon );
                    assert( r_h = PLD.gir );
                }else
                if( SE4_LEF == se4 
                ||  SE4_RIG == se4  
                ){
                    assert( r_w = PLD.gir );
                    assert( r_h = PLD.lon );
                }else{
                    ERR("[BAD_SE4]");
                };;
            //:==================:GET_TETCAKE_EDGE_BUFFER://
            //:ADD_LAYER_TO_TETCAKE_EDGE:================://

            //: @dfe@ : Distance From Edge When ://
            //:         Copying Line Into Edge  ://
            dfe =( p16 ); 
            assert( dfe >= 0 && dfe <=(PLD.gir-1) );

            a3f_shell64_copline_pix_nop_r_w_r_h_se4_dfe___pix(
                                TET,NOP,r_w,r_h,se4,dfe,  CAK
            );;
            
            //:#_TETCAKE_HAS_BILATERAL_SYMMETRY_#://
            a3f_shell64_ass_cak(CAK,r_w,r_h,se4,"[ASS_CAK]");

            //:================:ADD_LAYER_TO_TETCAKE_EDGE://

        };; //:NEXT[ p16 ]:::::::::::::::::::::::::::::::::::://

        //:a3f_shell64_ass_cak(tetcake,r_w,r_h,se4,"[tetcake]");

        };; //:NEXT[ se4 ]:::::::::::::::::::::::::::::::::::://

    #undef  B_0  //: BAMSHEL#1    :--------------------------://
    #undef  B_1  //: BAMSHEL#2    :--------------------------://
    #undef  TET  //: TETSHEL      :--------------------------://
    #undef  CAK  //: TETCAKE/EDGE :--------------------------://
    #undef  NOP  //: Num_Pixels   :--------------------------://
    }

    VOD a3f_shell64_tetcake_finfram( VOD ){
        //:----------------------------------------------://
        //:[ ATF ]: Takes all of the[ TETCAKE ]edge      ://
        //:         buffers and composites them into     ://
        //:         SHELL64's final [ 64_x_64 ]output    ://
        //:         buffer. This is the final step       ://
        //:         in creating a frame graphic          ://
        //:         for our HENTAIF tiles.               ://
        //:----------------------------------------------://

        //:----------------------------------------------://
        //:                                              ://
        //: |<------------ 064 ------------>|            ://
        //: |<-016->|<---- 032 ---->|<-016->|            ://
        //: +---=---+---=---+---=---+---=---+            ://
        //: | \     |               |     / |            ://
        //: |   \   |      TOP      |   /   |            ://
        //: |     \ |     img_y     | /     |            ://
        //: +---=---+---=---+---=---+---=---+            ://
        //: |       |               |       |            ://
        //: |       |               |       |            ://
        //: |       |               |       |            ://
        //: +  LEF  +               +  RIG  +            ://
        //: |       |               |       |            ://
        //: | img_x |               | img_x |            ://
        //: |       |               |       |            ://
        //: +---=---+---=---+---=---+---=---+            ://
        //: |     / |               | \     |            ://
        //: |   /   |      BOT      |   \   |            ://
        //: | /     |     img_y     |     \ |            ://
        //: +---=---+---=---+---=---+---=---+            ://
        //:                                              ://
        //: +---=---+                                    ://
        //: | \     | TOP_LEF : 50/50 mix at 45 DEG      ://
        //: |   \   |   O_R                              ://
        //: |     \ | BOT_RIG : 50/50 mix at 45 DEG      ://
        //: +---=---+                                    ://
        //:                                              ://
        //: +---=---+                                    ://
        //: |     / | TOP_RIG : 50/50 mix at 45 DEG      ://
        //: |   /   |   O_R                              ://
        //: | /     | BOT_LEF : 50/50 mix at 45 DEG      ://
        //: +---=---+                                    ://
        //:                                              ://
        //:          NOTE:                               ://
        //:                img_x = ( LEF | RIG )         ://
        //:                img_y = ( TOP | BOT )         ://
        //:                                              ://
        //:----------------------------------------------://

        U08* pix = &( PLD.o_5.pix[ 0 ]            );
        U08* lef = &( PLD.e_4[ SE4_LEF ].pix[ 0 ] );
        U08* rig = &( PLD.e_4[ SE4_RIG ].pix[ 0 ] );
        U08* top = &( PLD.e_4[ SE4_TOP ].pix[ 0 ] );
        U08* bot = &( PLD.e_4[ SE4_BOT ].pix[ 0 ] );

        I32 lon =( PLD.lon ); //:long  axis  : 64 pixels ://
        I32 gir =( PLD.gir ); //:girth axis  : 16 pixels ://

        I32 wid =( PLD.san ); //: output frame image is  ://
        I32 hig =( PLD.san ); //: a 64_x_64 square       ://

        //:----------------------------------------------://
        //: Working on getting away from 64 & 16 hardcode://
        //: HOWEVER! Input should be power of 2 and girth://
        //: axis is always 1/4th of span.                ://
        //:----------------------------------------------://
        assert( PLD.gir == PLD.lon / 4 );
        assert( PLD.san == PLD.lon * 1 );
        A3A_PO2_I32( PLD.san , "[PLD.san]" );//:PowerOf2?://
    
        A3F_SHELL64_EFRAMER_TOP_BOT_LEF_RIG_LON_GIR_PIX_WID_HIG(
                            top,bot,lef,rig,lon,gir,pix,wid,hig)
        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    }

//:========================================:TETCAKE_FUNCTIONS://
//:COMMENTS_SINCE_REFACTORING_INTO_OWN_FILE:=================://
/** ******************************************************** ***

    #_TETCAKE_HAS_BILATERAL_SYMMETRY_#

        Because The TETCAKE is made layer by symmetric 
        layer, we can test for symmetry before the     
        entire thing is made. This actually gives      
        us better feedback because I am noticing       
        the first row is... not where it should be.    


*** ******************************************************** **/
//:=================:COMMENTS_SINCE_REFACTORING_INTO_OWN_FILE://

