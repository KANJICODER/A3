//:FILE_MACROS:==============================================://
#define     S_A               _Static_assert ///////////////////

#define SE4_TOP                              a3m_shell64_se4_top
#define SE4_LEF                              a3m_shell64_se4_lef
#define SE4_RIG                              a3m_shell64_se4_rig
#define SE4_BOT                              a3m_shell64_se4_bot

#define RC4_000                              a3m_shell64_rc4_000
#define RC4_090                              a3m_shell64_rc4_090
#define RC4_180                              a3m_shell64_rc4_180
#define RC4_270                              a3m_shell64_rc4_270
 
#define UM3_100                              a3m_shell64_um3_100
#define UM3_075                              a3m_shell64_um3_075
#define UM3_025                              a3m_shell64_um3_025
 
#define BAM_4_X                              a3m_shell64_bam_4_x
#define BAM_1_X                              a3m_shell64_bam_1_x
#define TET_NOP                              a3m_shell64_tet_nop
       
#define BAM_INN                              a3m_shell64_bam_inn
#define BAM_SOS                              a3m_shell64_bam_sos
#define BAM_MIR                              a3m_shell64_bam_mir
#define BAM_OUT                              a3m_shell64_bam_out

#define TOP_WID                              a3m_shell64_top_wid
#define TOP_HIG                              a3m_shell64_top_hig            
#define LEF_WID                              a3m_shell64_lef_wid
#define LEF_HIG                              a3m_shell64_lef_hig                  
#define RIG_WID                              a3m_shell64_rig_wid
#define RIG_HIG                              a3m_shell64_rig_hig                
#define BOT_WID                              a3m_shell64_bot_wid
#define BOT_HIG                              a3m_shell64_bot_hig

#define BAM_NOP_INN                      a3m_shell64_bam_nop_inn
#define BAM_NOP_SOS                      a3m_shell64_bam_nop_sos
#define BAM_NOP_MIR                      a3m_shell64_bam_nop_mir
#define BAM_NOP_OUT                      a3m_shell64_bam_nop_out
                                         
#define BAM_U8B_INN                      a3m_shell64_bam_u8b_inn
#define BAM_U8B_SOS                      a3m_shell64_bam_u8b_sos
#define BAM_U8B_MIR                      a3m_shell64_bam_u8b_mir
#define BAM_U8B_OUT                      a3m_shell64_bam_u8b_out

//:==============================================:FILE_MACROS://

    #define a3m_shell64_save_unit_test_bitmaps_to_disk ( 1 )


    //:SE4 : Side_Enumeration[ 0|1|2|3 ] ://
    //:      Lets us know which edge of the
    //:      tile we are currently fabricating.

    #define a3m_shell64_se4_top ( 0 )
    #define a3m_shell64_se4_lef ( 1 )
    #define a3m_shell64_se4_rig ( 2 )
    #define a3m_shell64_se4_bot ( 3 )

    //:RC4 : Rotation Code ( [0|1|2|3] )

    #define a3m_shell64_rc4_000 ( 0 )
    #define a3m_shell64_rc4_090 ( 1 )
    #define a3m_shell64_rc4_180 ( 2 )
    #define a3m_shell64_rc4_270 ( 3 )

    //:UM4 : Unshelling Method( [0|1|2] )

    #define a3m_shell64_um3_100 ( 0 )
    #define a3m_shell64_um3_075 ( 1 )
    #define a3m_shell64_um3_025 ( 2 )

    //:BAMSHELL LINE BUFFER SIZES://
    
    #define a3m_shell64_bam_4_x ( 64 * 4 * 4 ) //:[@BAM_4_X@]://
    #define a3m_shell64_bam_1_x ( 64 * 1 * 4 ) //:[@BAM_1_X@]:// 
                                               
    #define a3m_shell64_bam_u8b_inn ( 64 * 4 * 4 )   
    #define a3m_shell64_bam_u8b_sos ( 64 * 1 * 4 )   
    #define a3m_shell64_bam_u8b_mir ( 64 * 1 * 4 )   
    #define a3m_shell64_bam_u8b_out ( 64 * 1 * 4 )  

    #define a3m_shell64_bam_nop_inn ( 64 * 4     )
    #define a3m_shell64_bam_nop_sos ( 64 * 1     )
    #define a3m_shell64_bam_nop_mir ( 64 * 1     )
    #define a3m_shell64_bam_nop_out ( 64 * 1     )

        S_A( BAM_U8B_INN / 4 == BAM_NOP_INN , "[U8B:NOP:INN]" );
        S_A( BAM_U8B_SOS / 4 == BAM_NOP_SOS , "[U8B:NOP:SOS]" );
        S_A( BAM_U8B_MIR / 4 == BAM_NOP_MIR , "[U8B:NOP:MIR]" );
        S_A( BAM_U8B_OUT / 4 == BAM_NOP_OUT , "[U8B:NOP:OUT]" );

    //:TETSHEL LINE BUFFER SIZES:

    #define a3m_shell64_tet_nop ( 64 * 1 ) 
    S_A( TET_NOP == (BAM_1_X/4) , "[TET_NOP:NE:BAM_1_X]" );

    //:tetcake_edge_buffer_sizes:----------------------------://
        //:                      :                           ://
        //:FOR[ a3d_shell64_top ]:  These 4 buffers are the  ://
        //:FOR[ a3d_shell64_lef ]:  4 TETCAKE images that    ://
        //:FOR[ a3d_shell64_rig ]:  are blended together     ://
        //:FOR[ a3d_shell64_bot ]:  to create FINFRAM        ://
        //:                      :          +----------------://
        #define a3m_shell64_top [A3D:NOT:A3M]
        #define a3m_shell64_lef [A3D:NOT:A3M]
        #define a3m_shell64_rig [A3D:NOT:A3M]
        #define a3m_shell64_bot [A3D:NOT:A3M]

        #define a3m_shell64_top_wid ( 64 )  //+--------------+//
        #define a3m_shell64_top_hig ( 16 )  //:   +-----+    ://
                                            //:   | TOP |    ://
        #define a3m_shell64_lef_wid ( 16 )  //: +-+-----+-+  ://
        #define a3m_shell64_lef_hig ( 64 )  //: | |     | |  ://
                                            //: |L|     |R|  ://
        #define a3m_shell64_rig_wid ( 16 )  //: | |     | |  ://
        #define a3m_shell64_rig_hig ( 64 )  //: +-+-----+-+  ://
                                            //:   | TOP |    ://
        #define a3m_shell64_bot_wid ( 64 )  //:   +-----+    ://
        #define a3m_shell64_bot_hig ( 16 )  //+--------------+//

        #define a3m_shell64_img_lon ( 64 ) //:LONG__AXIS://
        #define a3m_shell64_img_gir ( 16 ) //:GIRTH_AXIS://

        //:DIM[ TOP ]==DIM[ BOT ]://
        //:DIM[ LEF ]==DIM[ RIG ]://
        S_A( TOP_WID == BOT_WID , "[TOP_WID:NE:BOT_WID]" );
        S_A( TOP_HIG == BOT_HIG , "[TOP_HIG:NE:BOT_HIG]" );
        S_A( LEF_WID == RIG_WID , "[LEF_WID:NE:RIG_WID]" );
        S_A( LEF_HIG == RIG_HIG , "[LEF_HIG:NE:RIG_HIG]" );
    
        #define THK (16) //:THICKNESS://
        #define LON (64) //:LONGTH   ://

            S_A( THK == TOP_HIG , "[THK:NE:TOP_HIG]" );
            S_A( THK == BOT_HIG , "[THK:NE:BOT_HIG]" );
            S_A( THK == LEF_WID , "[THK:NE:LEF_WID]" );
            S_A( THK == RIG_WID , "[THK:NE:RIG_WID]" );

            S_A( LON == TOP_WID , "[LON:NE:TOP_WID]" );
            S_A( LON == BOT_WID , "[LON:NE:BOT_WID]" );
            S_A( LON == LEF_HIG , "[LON:NE:LEF_HIG]" );
            S_A( LON == RIG_HIG , "[LON:NE:RIG_HIG]" );

        #undef  THK
        #undef  LON

    //:----------------------------:tetcake_edge_buffer_sizes://

//:MONKEY_WRENCH_MACROS:=====================================://

    #define a3m_shell64_rot_000  [USE:rc4_000]
    #define a3m_shell64_rot_090  [USE:rc4_090]
    #define a3m_shell64_rot_180  [USE:rc4_180]
    #define a3m_shell64_rot_270  [USE:rc4_270]

    //:Was using this originally but realized it was
    //:ambigous and confusing the hell out of me.
    //:Now must specify[ u8b ]( U08_Byte_Count   )      @u8b@://
    //:              OR[ nop ]( Number_Of_Pixels )      @nop@://
    #define a3m_shell64_bam_inn  [PIK:bam_u8b_inn|bam_nop_inn] 
    #define a3m_shell64_bam_sos  [PIK:bam_u8b_sos|bam_nop_sos] 
    #define a3m_shell64_bam_mir  [PIK:bam_u8b_mir|bam_nop_mir] 
    #define a3m_shell64_bam_out  [PIK:bam_u8b_out|bam_nop_out]

    #define a3m_shell64_bam_nob_inn [nob_looks_like_nop]
    #define a3m_shell64_bam_nob_sos [nob_looks_like_nop]
    #define a3m_shell64_bam_nob_mir [nob_looks_like_nop]
    #define a3m_shell64_bam_nob_out [nob_looks_like_nop]

//:=====================================:MONKEY_WRENCH_MACROS://
/** ******************************************************** ***
*** COMMENTS_ARE_READ_LAST_OR_NEVER ************************ ***

    @BAM_4_X@ : Bamshell Buffer Line ( 4X of 64 )
                Big enough to hold all 4 of the largest
                edges of 64x64 tile when unshelling.
                
                When taking full outer shell of 64x64 
                graphic, we get a line that
                is ( 64 + 64 + 64 + 64 - 1 - 1 - 1 - 1 )
                   (  (64*4) - ( 4 ) )
                   (  (256 ) - ( 4 ) )
                   (   254 )
                254 pixels long.

    @BAM_1_X@ : All other buffers are 64 pixels long
                because 64 pixels is the size of the
                edges on the 64x64 tiles.

    @BAM_INN@ : The initial unshelled input buffer.

    @BAM_SOS@ : SQUASHED OR STRETECHED.
                ONESHEL #1 goes into this buffer.
                ONESHEL #1 is_the_contents_of_this_buffer.

    @BAM_MIR@ : MIRrrored[ BAM_SOS ]buffer.
                ONESHEL #2 is_the_contents_of_this_buffer.
                
    @BAM_OUT@ : The output buffer where we composite    
                @BAM_SOS@ 's ONESHEL #1
            AND @BAM_MIR@ 's ONESHEL #2
            
            TO CREATE THE FINAL @BAMSHEL@

    @BAMSHEL@ : Blended And Mirrored SHEL (shelline)
                The 1 pixel thick by 64 pixel long
                graphic that is derived from unshelling
                one shell of 64_x_64 graphic.

    @TETSHEL@ : Two BAMSHEL graphics that have been
                darken blended together. The hope is that
                the edges will have both:

                1. DARKNESS   ( Edges Are Darker Than Body )
                2. STRIATIONS ( WOOD GRAIN )

                Striations because we blend every two
                lines together. It would be kind of like
                if we bilinearlly filtered an image only
                by looking at X (left and right) values
                but not  Y ( above and below ) values.
                The final bilinear filtered image would
                appear to have a horizontal grain because
                there will be more constrast on the axis
                that had no blending applied.

    @TETCAKE@ : A stack of[ TETSHEL ]lines creating
                either the[ TOP | LEF | RIG | BOT ]
                edge of our[ FINFRAM ]graphic.
    
    @FINFRAM@ : The final 64x64 frame graphic that we can
                use to composite over our HENTAIF mirror
                tiles to create a very polished auto 
                generated tile design.

*** ************************ COMMENTS_ARE_READ_LAST_OR_NEVER ***
*** ******************************************************** **/
//:FILE_MACROS:==============================================://
#undef      S_A  //:-----------------------------------------://
                 //:-----------------------------------------://
#undef  SE4_TOP  //:-----------------------------------------://
#undef  SE4_LEF  //:-----------------------------------------://
#undef  SE4_RIG  //:-----------------------------------------://
#undef  SE4_BOT  //:-----------------------------------------://
                 //:-----------------------------------------://
#undef  RC4_000  //:-----------------------------------------://
#undef  RC4_090  //:-----------------------------------------://
#undef  RC4_180  //:-----------------------------------------://
#undef  RC4_270  //:-----------------------------------------://
                 //:-----------------------------------------://
#undef  UM3_100  //:-----------------------------------------://
#undef  UM3_075  //:-----------------------------------------://
#undef  UM3_025  //:-----------------------------------------://
                 //:-----------------------------------------://
#undef  BAM_4_X  //:-----------------------------------------://
#undef  BAM_1_X  //:-----------------------------------------://
#undef  TET_NOP  //:-----------------------------------------://
                 //:-----------------------------------------://
#undef  BAM_INN  //:-----------------------------------------://
#undef  BAM_SOS  //:-----------------------------------------://
#undef  BAM_MIR  //:-----------------------------------------://
#undef  BAM_OUT  //:-----------------------------------------://
                 //:-----------------------------------------://
#undef  TOP_WID  //:-----------------------------------------://
#undef  TOP_HIG  //:-----------------------------------------://
#undef  LEF_WID  //:-----------------------------------------://
#undef  LEF_HIG  //:-----------------------------------------://
#undef  RIG_WID  //:-----------------------------------------://
#undef  RIG_HIG  //:-----------------------------------------://
#undef  BOT_WID  //:-----------------------------------------://
#undef  BOT_HIG  //:-----------------------------------------://
                        //:----------------------------------://
#undef  BAM_NOP_INN     //:----------------------------------://
#undef  BAM_NOP_SOS     //:----------------------------------://
#undef  BAM_NOP_MIR     //:----------------------------------://
#undef  BAM_NOP_OUT     //:----------------------------------://
                        //:----------------------------------://
#undef  BAM_U8B_INN     //:----------------------------------://
#undef  BAM_U8B_SOS     //:----------------------------------://
#undef  BAM_U8B_MIR     //:----------------------------------://
#undef  BAM_U8B_OUT     //:----------------------------------://

//:==============================================:FILE_MACROS://