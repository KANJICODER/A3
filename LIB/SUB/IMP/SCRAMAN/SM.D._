//:ABOUT_SCRAMAN:============================================://
//:                                                          ://
//:     SEE[ SM.T._ ]For_About_Summary.                      ://
//:     THIS_FILE[ SM.D._ ]( D : Data )                      ://
//:                                                          ://
//:============================================:ABOUT_SCRAMAN://
//:FILE_MACROS:==============================================://
#define I64 int64_t                 //:----------------------://
#define I32 int32_t                 //:----------------------://
#define I08  int8_t                 //:----------------------://
#define U08 uint8_t                 //:----------------------://
#define CHR const char              //:----------------------://
#define NCC       char              //:----------------------://
#define TRA struct A3T_SCRAMAN_TRA  //:----------------------://
#define CAP A3M_SCRAMAN_STK_CAP     //:----------------------://
#define S_A _Static_assert          //:----------------------://
//:==============================================:FILE_MACROS://
//:ADDED_FOR_DEBUGGING:======================================://

    //:--------------------------------------------------://
    //:  a3f_scraman_utm : UnitTestMain flag             ://
    //:                                                  ://
    //:  0 : Not currently running unit tests.           ://
    //:  1 : Is  currently running unit tests.           ://
    //:                                                  ://
    //:  Added so we can inline very expensive           ://
    //:  assertions into functions when we know          ://
    //:  we are running  unit tests.                     ://
    //:--------------------------------------------------://

    I32 a3d_scraman_utm =( 0 );     //:DATE[ 2022_04_13 ]://

    //:--------------------------------------------------://
    //: a3d_scraman_aos_enu : aos_enumeration            ://
    //: ------------------------------------------------ ://
    //: Keeps track of the last transform function       ://
    //: invoked by a transformation instruction.         ://
    //: Using this to find out why my ping-pong          ://
    //: buffers ( beg & end ) are getting corrupted.     ://
    //:--------------------------------------------------://
    
    I32 a3d_scraman_aos_enu =( 0 ); //:DATE[ 2022_04_13 ]://     
  

//:======================================:ADDED_FOR_DEBUGGING://
//:SCRAMAN_DATA:=============================================://

    //:--------------------------------------------------://
    //: "is_batch_processing" flag.                      ://
    //: Flag used to change behavior of image saving.    ://
    //: Specifically alters[ A3F_SCRAMAN_SAV_IMG ]       ://
    //:--------------------------------------------------://
    
        I08 a3d_scraman_yes_bat_run=( 0 ); //:IsBatching?://
        I32 a3d_scraman_yes_bat_dex=( 0 ); //:BatchIndex ://
        NCC a3d_scraman_yes_bat_buf[ 16 ]; //:STR_BUFfer ://

    //:--------------------------------------------------://
    //: Default Stack Of Transforms                      ://
    //: Populate it in the main initializer.             ://
    //:--------------------------------------------------://

        TRA A3D_SCRAMAN_DEF_STK[ CAP ]={ 0 };
        I32 A3D_SCRAMAN_DEF_LEN =(  0  );
        I32 A3D_SCRAMAN_DEF_CAP =( CAP );

    //:--------------------------------------------------://
    //: I don't feel like passing up this name param     ://
    //: through a shit tone of function calls.           ://
    //: Especially when it may or may not exist.         ://
    //:                                                  ://
    //: Sometimes global scope variables as inputs       ://
    //: are the right way to go. They probably only      ://
    //: feel ugly because[ oop ]and[ functional ]        ://
    //: purists want to make you think you are sinning.  ://
    //:--------------------------------------------------://

        I32  A3D_SCRAMAN_NAM_HAS =( ((I32 )0) );        
        CHR* A3D_SCRAMAN_NAM_STR =( ((CHR*)0) );  
        I64  A3D_SCRAMAN_TIM     =( 0 ); //:FakeTimer://

        //:Underlying Buffer For[ A3F_SCRAMAN_FIP_NAM ]  ://    
               NCC  A3D_SCRAMAN_FIP_NAM_BUF[ 256 ] ;
        S_A(   A3M_SCRAMAN_FIP_NAM_CAP    == 256 , "[FF]" );    

//:=============================================:SCRAMAN_DATA://
//:DATA_FOR:A3F_SCRAMAN_GEN:=================================://
//:                                                          ://
//:     Average Red  : Initial Offset Into[ ARR_ACT ]        ://
//:     Average Green: Initial Offset Into[ ARR_SAM ]        ://
//:     Average Blue : Constant Offset Applied to            ://
//:                  : values fetched from[ ARR_SAM ]        ://
//:                                                          ://
//:- - - - - - - - - - - - - - -- - - - - - - - - - - - - - -://
//:                                                          ://
//:  For scrambling and unscrambling images via unlockable   ://
//:  encoder+decoder in CHADGAM ( "pussyorputin" ),          ://
//:  we need a way to know how to scramble and how to        ://
//:  unscramble. One thing that does __NOT__ change when     ://
//:  image is scrambled is the AVERAGE COLOR OF PIXELS.      ://
//:                                                          ://
//:  The pixels have only moved place. So we can use         ://
//:  the average[ RED , GREEN , BLUE ]as psuedo-random       ://
//:  keys to help us generate the scrambling instructions    ://
//:  to be applied to the image.                             ://
//:  WRITTEN_DATE[ 2022_04_08 ]                              ://
//:                                                          ://
//:data_for:a3f_scraman_gen:- - - - - - - - - - - - - - - - -://


    S_A( 256 == A3M_SCRAMAN_RANDGEN_ARR_LEN , "[ARR_WID]" );

    #define A  ( 1 )  //:--------------------------------://
    #define B  ( 2 )  //:--------------------------------://
    #define C  ( 4 )  //:--------------------------------://
    #define D  ( 8 )  //:--------------------------------://
    #define E  (16 )  //:--------------------------------://
    #define F  (32 )  //:--------------------------------://

    //:VALID_SIZES[ A , B , C , D ,  E ,  F ]------------://
    //:VALID_SIZES[ 1 , 2 , 4 , 8 , 16 , 32 ]------------://
    I08 A3D_SCRAMAN_RANDGEN_ARR_WID[
        A3M_SCRAMAN_RANDGEN_ARR_LEN
    ]={
    //: 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6
        A,B,C,D,E,F,A,B,B,C,D,D,E,F,F,E  //:--------: 01 ://
    ,   A,B,B,B,D,D,E,A,B,F,E,E,F,C,B,A  //:--------: 02 ://
    ,   B,C,D,F,A,B,D,E,D,E,A,F,F,A,A,B  //:--------: 03 ://
    ,   C,A,C,F,A,C,C,A,F,F,E,E,D,D,E,F  //:--------: 04 ://
    ,   C,D,A,F,E,D,C,D,E,F,C,A,B,B,A,C  //:--------: 05 ://
    ,   C,F,E,D,E,D,D,F,A,D,D,A,D,B,A,C  //:--------: 06 ://
    ,   F,E,F,E,F,E,E,F,A,D,D,A,A,B,B,B  //:--------: 07 ://
    ,   E,F,A,B,C,B,C,D,C,D,E,F,C,D,A,A  //:--------: 08 ://
    ,   B,E,C,A,B,A,D,D,A,D,C,A,B,B,A,B  //:--------: 09 ://
    ,   B,E,C,C,A,D,A,D,D,A,F,A,D,D,A,C  //:--------: 10 ://
    ,   C,A,B,B,E,F,F,A,C,C,A,A,A,D,D,D  //:--------: 11 ://
    ,   D,D,B,E,B,E,D,D,D,C,A,B,A,D,A,F  //:--------: 12 ://
    ,   F,D,F,D,C,B,D,E,F,A,A,A,C,A,B,A  //:--------: 13 ://
    ,   D,A,B,B,D,A,B,B,A,B,B,A,B,B,A,E  //:--------: 14 ://    
    ,   B,A,E,D,A,E,A,E,E,E,B,B,B,B,A,E  //:--------: 15 ://
    ,   F,A,F,A,D,A,F,F,A,A,F,A,C,A,C,A  //:--------: 16 ://
    };

    //:VALID_SIZES[ A , B , C , D ,  E ,  F ]------------://
    //:VALID_SIZES[ 1 , 2 , 4 , 8 , 16 , 32 ]------------://
    I08 A3D_SCRAMAN_RANDGEN_ARR_HIG[
        A3M_SCRAMAN_RANDGEN_ARR_LEN
    ]={
    //: 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6
        F,B,D,D,E,E,E,F,F,F,A,B,E,F,A,B  //:--------: 01 ://
    ,   F,B,A,D,E,C,A,C,A,D,A,B,B,A,C,A  //:--------: 02 ://
    ,   F,A,F,A,F,A,A,A,A,C,D,A,F,D,A,A  //:--------: 03 ://
    ,   E,A,E,A,E,E,E,A,D,E,B,B,A,B,C,C  //:--------: 04 ://
    ,   A,B,C,B,E,B,F,B,B,F,B,E,B,C,A,F  //:--------: 05 ://
    ,   A,D,C,E,F,A,B,E,E,A,D,D,C,A,D,A  //:--------: 06 ://
    ,   E,A,C,F,B,C,D,A,F,F,A,A,F,F,D,D  //:--------: 07 ://
    ,   C,F,A,E,A,D,F,D,B,A,C,B,E,D,B,A  //:--------: 08 ://
    ,   D,A,B,D,A,A,C,B,B,A,E,D,A,D,A,D  //:--------: 09 ://    
    ,   C,D,D,A,D,C,C,A,A,A,D,B,B,A,A,C  //:--------: 10 ://
    ,   D,E,A,C,F,D,E,F,B,A,C,B,E,B,A,E  //:--------: 11 ://
    ,   E,B,A,D,A,D,D,C,D,A,F,B,D,B,A,F  //:--------: 12 ://
    ,   A,B,D,F,E,D,E,D,A,A,A,A,A,B,C,A  //:--------: 13 ://
    ,   A,C,D,C,D,F,E,A,B,D,D,D,C,D,C,B  //:--------: 14 ://
    ,   D,F,E,B,A,C,D,D,C,D,A,F,E,C,B,E  //:--------: 15 ://
    ,   A,D,F,E,F,E,E,D,E,E,D,B,E,E,D,A  //:--------: 16 ://
    };
    #undef  A  //:---------------------------------------://
    #undef  B  //:---------------------------------------://
    #undef  C  //:---------------------------------------://
    #undef  D  //:---------------------------------------://
    #undef  E  //:---------------------------------------://
    #undef  F  //:---------------------------------------://


    S_A( 256 == A3M_SCRAMAN_RANDGEN_ARR_LEN , "[ARR_ACT]" );

    #define V A3M_SCRAMAN_ENU_VER
    #define H A3M_SCRAMAN_ENU_HOR
    #define R A3M_SCRAMAN_ENU_RIN
    I32 A3D_SCRAMAN_RANDGEN_ARR_ACT[ 
        A3M_SCRAMAN_RANDGEN_ARR_LEN
    ]={
    //: 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6
        V,H,R,R,R,V,H,V,H,R,R,R,V,H,H,H  //:--------: 01 ://
    ,   V,V,R,R,V,V,H,H,V,V,R,V,R,H,V,R  //:--------: 02 ://
    ,   V,H,H,H,R,R,R,V,H,V,V,V,H,V,R,R  //:--------: 03 ://
    ,   H,V,V,V,V,R,R,R,V,H,R,V,R,H,H,H  //:--------: 04 ://
    ,   H,V,V,H,V,V,H,V,R,R,R,R,V,R,V,R  //:--------: 05 ://
    ,   V,H,R,R,H,V,R,H,V,H,V,V,H,H,V,V  //:--------: 06 ://
    ,   H,R,R,R,H,V,H,H,H,V,R,R,R,H,R,H  //:--------: 07 ://
    ,   R,R,H,H,R,R,H,H,R,R,R,H,V,H,V,R  //:--------: 08 ://
    ,   R,H,V,V,R,R,H,H,V,R,H,H,H,V,H,V  //:--------: 09 ://
    ,   H,V,H,R,V,R,V,H,V,H,H,V,V,H,H,R  //:--------: 10 ://
    ,   R,H,R,R,R,H,V,H,H,R,V,H,R,R,R,R  //:--------: 11 ://
    ,   V,H,V,V,V,H,H,V,V,V,H,V,R,R,H,H  //:--------: 12 ://
    ,   H,V,H,V,H,V,H,V,H,V,H,V,H,V,R,R  //:--------: 13 ://
    ,   H,H,H,H,H,H,H,H,R,R,R,R,R,R,R,R  //:--------: 14 ://
    ,   H,V,H,R,H,V,V,H,R,R,R,H,V,V,H,V  //:--------: 15 ://
    ,   H,V,V,V,V,H,V,V,V,V,R,H,V,V,H,R  //:--------: 16 ://
    };
    #undef  V
    #undef  H
    #undef  R

    S_A( 256 == A3M_SCRAMAN_RANDGEN_ARR_LEN , "[ARR_SAM]" );
    
    U08 A3D_SCRAMAN_RANDGEN_ARR_SAM[ 
        A3M_SCRAMAN_RANDGEN_ARR_LEN
    ]={
    //:   01   02   03   04   05   06   07   08
        0x99,0x27,0x59,0x9A,0x94,0xA6,0xA1,0x2A   //: 01 ://
    ,   0xA5,0x3A,0x32,0x61,0xD2,0x8F,0x4A,0x20   //: 02 ://
    ,   0x29,0xCC,0x0D,0xC7,0x3C,0x8B,0x8C,0xA0   //: 03 ://
    ,   0xD8,0x9E,0x45,0x98,0x19,0x70,0xDC,0x86   //: 04 ://
    ,   0x50,0xB2,0x74,0x99,0x94,0xB5,0xD1,0x4D   //: 05 ://
    ,   0x20,0x74,0xD5,0xD5,0xF9,0x64,0x63,0x03   //: 06 ://
    ,   0x98,0x9A,0xFF,0xFF,0xFF,0xFF,0xC0,0x7E   //: 07 ://
    ,   0xF3,0x90,0xC0,0x0C,0x38,0x2B,0x6E,0xA4   //: 08 ://
    
    ,   0xA6,0x28,0x3E,0x64,0x07,0xC7,0x6A,0xFA   //: 01 ://
    ,   0xBE,0xCE,0xF4,0xFC,0x85,0x34,0x95,0xCB   //: 02 ://
    ,   0x09,0xCE,0xF3,0xCF,0x5E,0x76,0xEC,0x05   //: 03 ://
    ,   0x2A,0xE6,0x94,0x36,0x19,0x88,0xBA,0x6A   //: 04 ://
    ,   0xD9,0x6A,0xFA,0x6E,0xE9,0xFC,0xBB,0x62   //: 05 ://
    ,   0x4F,0xD5,0x95,0x76,0x03,0xC2,0xD4,0xEF   //: 06 ://
    ,   0x79,0xBB,0x77,0x21,0xD9,0x14,0xEC,0xBB   //: 07 ://
    ,   0xF7,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF   //: 08 ://
    
    ,   0xD0,0x4C,0xE5,0xBA,0x2C,0x31,0x95,0x73   //: 01 ://
    ,   0xFF,0xFC,0xE4,0x57,0xB3,0xFF,0xD5,0xC9   //: 02 ://
    ,   0xFE,0x27,0xE5,0x30,0xF8,0x60,0x4E,0x1C   //: 03 ://
    ,   0x8E,0xC5,0x01,0x20,0x86,0x40,0x8C,0x73   //: 04 ://
    ,   0xFF,0xF3,0x92,0xC0,0x23,0x39,0x53,0xDE   //: 05 ://
    ,   0x81,0xE3,0x23,0x24,0x0E,0x5F,0xC9,0xD9   //: 06 ://
    ,   0xB8,0x3C,0x17,0x03,0xD0,0x2E,0x6B,0xA2   //: 07 ://
    ,   0x25,0xDB,0x1E,0x48,0x86,0x28,0x28,0xA4   //: 08 ://
    
    ,   0xDC,0xF2,0xC3,0x7E,0xFF,0x51,0x59,0xDF   //: 01 ://
    ,   0x4C,0xAD,0x51,0xF2,0x81,0x92,0x25,0x52   //: 02 ://
    ,   0xD5,0xD3,0x51,0xF5,0x5F,0xCD,0x65,0xB3   //: 03 ://
    ,   0x42,0x30,0xF2,0x39,0xBA,0x5D,0x65,0x0E   //: 04 ://
    ,   0x1E,0xDC,0x3A,0xB6,0xD8,0xD1,0x9A,0x12   //: 05 ://
    ,   0xF3,0xAA,0x2C,0xCB,0x11,0x5B,0xCF,0xAD   //: 06 ://
    ,   0x35,0x52,0xE9,0x14,0xA9,0xD4,0x79,0xBC   //: 07 ://
    ,   0xE4,0xDC,0xA2,0x64,0x6A,0x51,0x3E,0x65   //: 08 ://
    };

//:=================================:DATA_FOR:A3F_SCRAMAN_GEN://
//:NAME_GENERATION_BUFFER:===================================://

    NCC     A3D_SCRAMAN_BUF_NAM[ 32 ]={ 0 }; //: NAME  ://
    NCC     A3D_SCRAMAN_BUF_DIG[ 32 ]={ 0 }; //: DIGITS://
    #define A3D_SCRAMAN_BUF_NUM [ YOU_WANT : BUF_DIG ]

//:===================================:NAME_GENERATION_BUFFER://
//:FILE_MACROS:==============================================://
#undef  I64   //:--------------------------------------------://
#undef  I32   //:--------------------------------------------://
#undef  I08   //:--------------------------------------------://
#undef  U08   //:--------------------------------------------://
#undef  CHR   //:--------------------------------------------://
#undef  NCC   //:--------------------------------------------://
#undef  TRA   //:--------------------------------------------://
#undef  CAP   //:--------------------------------------------://
#undef  S_A   //:--------------------------------------------://
//:==============================================:FILE_MACROS://
//: COMMENTS_ARE_READ_LAST_OR_NEVER ************************ ://
/** !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ************************ ***
*** COMMENTS_ARE_READ_LAST_OR_NEVER ************************ ***

    @DEF_STK@ : Default Stack Of Transforms
    @DEF_LEN@ : DEF_STK's length (number of transforms)
    @NAM_HAS@ : Name.has
    @NAM_STR@ : Name.string

*** ************************ COMMENTS_ARE_READ_LAST_OR_NEVER ***
*** ************************ !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! **/
//: ************************ COMMENTS_ARE_READ_LAST_OR_NEVER ://