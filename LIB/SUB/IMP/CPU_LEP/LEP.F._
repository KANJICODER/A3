//:CPU_LEP_FILE_MACROS:======================================://

    #define U08         uint8_t
    #define U32        uint32_t
    #define I32         int32_t
    #define VOD            void
    #define DMD struct  A3T_DMD
    #define REC struct  A3T_REC
    #define ERR a3f_cpu_lep_err
    #define LOG a3f_cpu_lep_log
    #define N2D struct  A3T_N2D
    #define N4D struct  A3T_N4D
    #define L80 struct  A3T_L80
    #define CAM struct  A3T_CAM

//:======================================:CPU_LEP_FILE_MACROS://
//:CPU_LEP_FUNCTIONS:========================================://


    VOD A3F_CPU_LEP_INI( VOD ){
    
        //:Nothing Here Yet
        
    }
    VOD A3F_CPU_LEP_KIL( VOD ){
    
        //:Nothing Here Yet
        
    }
    
    I32 a3f_cpu_lep_pin_var(
        U08 a_0_t_1      //:Active_Zero_Torpid_1
    ,   I32 vin_loc      //:VariableIndex/AKA/Location
    ){
    /** ************************************************ ***
    *** We are going to return the global pixel index    ***
    *** of variable instead of the component index       ***
    *** because will (think) yeild more readable code.   ***
    ***                                                  ***
    *** Specifically because only[ ALPHA ]channel of     ***
    *** pixel is useful to us, which could make code     ***
    *** confusing if we are not careful with how we      ***
    *** write this.                                      ***
    *** ************************************************ **/
    
        I32 pin_var =( 0-666 );    //:Pixel_INdex:of:VARiable://
    
        //:Abstract as 32x512 grid of variables:
        
            I32 strip_y = vin_loc / (    1    * 32 ) ;
            I32 strip_x = vin_loc - ( strip_y * 32 ) ;
        
            if( A3C_BUG >= 1 ){
                if( 0
                || strip_x < 0 || strip_x > ( 32-1)   
                || strip_y < 0 || strip_y > (512-1)   
                ){
                    printf("[strip_x]:(%d)\n" , strip_x );
                    printf("[strip_y]:(%d)\n" , strip_y );
                    ERR("[OOB_STRIP]");
                };;
            };;
        
        //:Find Pixel Location Of This Cell:
        //:Local XY Coordinate.
        
            I32 loc_p_x =( strip_x * 16 );
            I32 loc_p_y =( strip_y *  1 );
            
        //:Get global pixel location on PIX2048 buffer:
        
            REC* rec = A3F_TAUMAST_GET_LEP( a_0_t_1 );
            I32 glo_p_x = ( ((I32)rec->x_0) + loc_p_x );
            I32 glo_p_y = ( ((I32)rec->y_0) + loc_p_y );
            
        //:Global Pixel XY Converted To Pixel Index:
        
            pin_var = ( 1 * ( glo_p_x + ( glo_p_y * 2048 ) ));
        
        return( pin_var );
    }
    I32 a3f_cpu_lep_cin_var(
        U08 a_0_t_1      //:Active_Zero_Torpid_1
    ,   I32 vin_loc      //:VariableIndex/AKA/Location
    ){
    /** ************************************************ ***
    *** Get component index of variable. Unlike 99% of   ***
    *** code in code base. This returns the[ ALPHA ]     ***
    *** channel byte, not the red channel byte.          ***
    *** ************************************************ **/
        I32 pin_var = a3f_cpu_lep_pin_var( a_0_t_1 , vin_loc );
        I32 cin_var =(
            ( 4 * pin_var ) 
        +     0 //:RED   ( PLUS ZERO , already here   )
        +     1 //:GREEN ( PLUS ONE  , moves to green )
        +     1 //:BLUE  ( PLUS ONE  , moves to blue  )
        +     1 //:ALPHA ( PLUS ONE  , moves to alpha )
        );;
        return( cin_var );
    }
    
    VOD A3F_CPU_LEP_PUT_VAR( 
        U08 a_0_t_1      //:Active_Zero_Torpid_1
    ,   I32 vin_loc      //:VariableIndex/AKA/Location
    ,   U32 var_val      //:VariableValue
    ){
        I32 cin_var = a3f_cpu_lep_cin_var( a_0_t_1 , vin_loc );
 
        #define P   A3D_PIX2048         //:Master_Pixels_Data://
        #define C       cin_var         //:Component____INdex://
        #define M           0x3         //:Mask_Of_2_Set_Bits://
        #define M32   ((U32)0x3)
        #define V       var_val         //:Variable_____Value://
            
        //:Erase The Bits We Are About To OverWrite:
        
            U08 d = ((U08)(~( M ) )); //:d is for "delete"
            assert( (U08)252 == d );
            
            //:Because "|=" is not friendly with pedantic
            //:casting.
            #define P00 A3D_PIX2048[ C + ( 0 *4 ) ]
            #define P01 A3D_PIX2048[ C + ( 1 *4 ) ]
            #define P02 A3D_PIX2048[ C + ( 2 *4 ) ]
            #define P03 A3D_PIX2048[ C + ( 3 *4 ) ]
            #define P04 A3D_PIX2048[ C + ( 4 *4 ) ]
            #define P05 A3D_PIX2048[ C + ( 5 *4 ) ]
            #define P06 A3D_PIX2048[ C + ( 6 *4 ) ]
            #define P07 A3D_PIX2048[ C + ( 7 *4 ) ]
            #define P08 A3D_PIX2048[ C + ( 8 *4 ) ]
            #define P09 A3D_PIX2048[ C + ( 9 *4 ) ]
            #define P10 A3D_PIX2048[ C + (10 *4 ) ]
            #define P11 A3D_PIX2048[ C + (11 *4 ) ]
            #define P12 A3D_PIX2048[ C + (12 *4 ) ]
            #define P13 A3D_PIX2048[ C + (13 *4 ) ]
            #define P14 A3D_PIX2048[ C + (14 *4 ) ]
            #define P15 A3D_PIX2048[ C + (15 *4 ) ]
        
                P00 &= ( d ); //:------------------://
                P01 &= ( d ); //:  using last      ://
                P02 &= ( d ); //:  2 pixels        ://
                P03 &= ( d ); //:  of each         ://
                P04 &= ( d ); //:  alpha byte      ://
                P05 &= ( d ); //:  so the          ://
                P06 &= ( d ); //:  [ del ]         ://
                P07 &= ( d ); //:  does NOT        ://
                P08 &= ( d ); //:  have to be      ://
                P09 &= ( d ); //:  shifted         ://
                P10 &= ( d ); //:  into            ://
                P11 &= ( d ); //:  place.          ://
                P12 &= ( d ); //:------------------://
                P13 &= ( d );
                P14 &= ( d );
                P15 &= ( d );
                
            //:Overwrite last 2 bits of each alpha channel byte:
            //: "|=" does not work with pedantic compilation.
            
                P00 =(U08)( P00 | ((U08)( M32& (V >>(2*15)) )));
                P01 =(U08)( P01 | ((U08)( M32& (V >>(2*14)) )));
                P02 =(U08)( P02 | ((U08)( M32& (V >>(2*13)) )));
                P03 =(U08)( P03 | ((U08)( M32& (V >>(2*12)) )));
                P04 =(U08)( P04 | ((U08)( M32& (V >>(2*11)) )));
                P05 =(U08)( P05 | ((U08)( M32& (V >>(2*10)) )));
                P06 =(U08)( P06 | ((U08)( M32& (V >>(2* 9)) )));
                P07 =(U08)( P07 | ((U08)( M32& (V >>(2* 8)) )));
                P08 =(U08)( P08 | ((U08)( M32& (V >>(2* 7)) )));
                P09 =(U08)( P09 | ((U08)( M32& (V >>(2* 6)) )));
                P10 =(U08)( P10 | ((U08)( M32& (V >>(2* 5)) )));
                P11 =(U08)( P11 | ((U08)( M32& (V >>(2* 4)) )));
                P12 =(U08)( P12 | ((U08)( M32& (V >>(2* 3)) )));
                P13 =(U08)( P13 | ((U08)( M32& (V >>(2* 2)) )));
                P14 =(U08)( P14 | ((U08)( M32& (V >>(2* 1)) )));
                P15 =(U08)( P15 | ((U08)( M32& (V >>(2* 0)) )));
                                                            
                
            #undef  P00
            #undef  P01
            #undef  P02
            #undef  P03
            #undef  P04
            #undef  P05
            #undef  P06
            #undef  P07
            #undef  P08
            #undef  P09
            #undef  P10
            #undef  P11
            #undef  P12
            #undef  P13
            #undef  P14
            #undef  P15
                
        #undef    P    //: P   : Pixel     Array       ://
        #undef    C    //: C   : Component Index       ://
        #undef    M    //: M   : 2    bit   mask       ://
        #undef    M32  //: M32 : M as U32
        #undef    V    //: V   : Value  To Store       ://
        
        return; //:RETURN_NOTHING_ITS_A_PUTTER://
    }
    
    U32 A3F_CPU_LEP_GET_VAR( 
        U08 a_0_t_1      //:Active_Zero_Torpid_1
    ,   I32 vin_loc      //:VariableIndex/AKA/Location
    ){
        U32 var_val =( (U32)0 ); //:VariableValue
        I32 cin_var = a3f_cpu_lep_cin_var( a_0_t_1 , vin_loc );
 
        #define PIX A3D_PIX2048         //:Master_Pixels_Data://
        #define CIN cin_var             //:Component____INdex://
        #define B11 0x3                 //:Mask_Of_2_Set_Bits://
        
            //:----------------------------------------------://
            //: No shifting out needed because already the   ://
            //: last 2 bits of the alpha channel.            ://
            //: But still need to shit into place for        ://
            //: the uint32 we are creating.                  ://
            //:----------------------------------------------://
            
            var_val =( ((U32)0)
            |(  (( PIX[ CIN + ( 0 *4 ) ] )&( B11 )) <<(2*15)  )
            |(  (( PIX[ CIN + ( 1 *4 ) ] )&( B11 )) <<(2*14)  )
            |(  (( PIX[ CIN + ( 2 *4 ) ] )&( B11 )) <<(2*13)  )
            |(  (( PIX[ CIN + ( 3 *4 ) ] )&( B11 )) <<(2*12)  )
            |(  (( PIX[ CIN + ( 4 *4 ) ] )&( B11 )) <<(2*11)  )
            |(  (( PIX[ CIN + ( 5 *4 ) ] )&( B11 )) <<(2*10)  )
            |(  (( PIX[ CIN + ( 6 *4 ) ] )&( B11 )) <<(2* 9)  )
            |(  (( PIX[ CIN + ( 7 *4 ) ] )&( B11 )) <<(2* 8)  )
            |(  (( PIX[ CIN + ( 8 *4 ) ] )&( B11 )) <<(2* 7)  )
            |(  (( PIX[ CIN + ( 9 *4 ) ] )&( B11 )) <<(2* 6)  )
            |(  (( PIX[ CIN + (10 *4 ) ] )&( B11 )) <<(2* 5)  )
            |(  (( PIX[ CIN + (11 *4 ) ] )&( B11 )) <<(2* 4)  )
            |(  (( PIX[ CIN + (12 *4 ) ] )&( B11 )) <<(2* 3)  )
            |(  (( PIX[ CIN + (13 *4 ) ] )&( B11 )) <<(2* 2)  )
            |(  (( PIX[ CIN + (14 *4 ) ] )&( B11 )) <<(2* 1)  )
            |(  (( PIX[ CIN + (15 *4 ) ] )&( B11 )) <<(2* 0)  )
            );;
        
        #undef  PIX
        #undef  CIN
        #undef  B11
        
        return( var_val );
    }
    
    VOD a3f_cpu_lep_pog_var(   //: Put_Or_Get variable       ://
        U08 put_get            //: put or get?               ://
    ,   U08 a_0_t_1            //: 0 == active , 1 == torpid ://
    ,   I32 vin_loc            //: VariableINdex_LOcation    ://
    ,   U32  *r_var            //: variable to get or put.   ://
    ){
    #define P_V A3F_CPU_LEP_PUT_VAR
    #define G_V A3F_CPU_LEP_GET_VAR
    
        switch( put_get ){
            case A3M_PUT : 
                           P_V( a_0_t_1 , vin_loc , (*r_var) );
            break;
            case A3M_GET :
                (*r_var) = G_V( a_0_t_1 , vin_loc );
            break;
            default:ERR("[2021_08_20]");
        }
    #undef  P_V
    #undef  G_V
    }
    
//:========================================:CPU_LEP_FUNCTIONS://
//:cpu_lep_cam_getter_and_putter:============================://

//:private_file_section(lowercase)://

  VOD a3f_cpu_lep_pog_a01_bas_cam(  
        U08  pog  //:Put_Or_Get, __NOT__ Pane_Of_Glass
    ,   U08  a01  //: 0 == active , 1 == torpid
    ,   I32  bas  //:Base Address Of Strip Of Data
    ,   CAM* cam  //:camera being putten or gotten
    ){
    /** ************************************************ ***
    *** Puts or gets a camera object.                    ***
    *** used by:                                         ***
    *** 1. A3F_CPU_LEP_GET_CAM                           ***
    *** 2. A3F_CPU_LEP_PUT_CAM                           ***
    ***                                                  ***
    *** Reason For Function:                             ***
    *** Makes sure the location of [GET|PUT] data is the ***
    *** same by having 1 function managing it adn only   ***
    *** having to write it once.                         ***
    *** ************************************************ **/
    #define F a3f_cpu_lep_pog_var
    #define C (*cam)
    
        //:Start At Base Address Of Strip Of Member
        //:Variables and write the vars:
       
        F(pog,a01,bas+  A3M_LEP_CAM_CAM_TYP  , &( C.cam_typ ) );
        F(pog,a01,bas+  A3M_LEP_CAM_OUT_WID  , &( C.out_wid ) );
        F(pog,a01,bas+  A3M_LEP_CAM_OUT_HIG  , &( C.out_hig ) );
        F(pog,a01,bas+  A3M_LEP_CAM_INN_WID  , &( C.inn_wid ) );
        F(pog,a01,bas+  A3M_LEP_CAM_INN_HIG  , &( C.inn_hig ) );
     
    #undef  F
    #undef  C
    }
    
    VOD a3f_cpu_lep_pog_cam( //:get or put camera object.
        U08     pog
    ,   U08     a01
    ,   N4D*    n4d
    ,   CAM*    cam
    ){
        //: Because I am a scared paranoid little person:
        
            assert( ((CAM*)0) != cam );
            assert( ((N4D*)0) != n4d );
    
        //:Convert n4d to a linearized [room/level] index:
        
            L80      l80 = {   0 }; 
            L80* ptr_l80 =&( l80 );
            A3F_CONVENG_N4D_L80( n4d , &( ptr_l80 ) );
        
        //:Get base address of strip of room data:
        
            assert( (sizeof(CAM)/4)  == A3M_LEP_CAM_SIM );
            I32 bas =( A3M_LEP_CAM_SIM * l80.lev_dex );
        
        //:get or put camera object:
        //:lower level call that knows the
        //:base address of data:
            
            a3f_cpu_lep_pog_a01_bas_cam( 
                        pog,a01,bas,cam);

        //:Nothing to return.
        return;
    }

//:============================:cpu_lep_cam_getter_and_putter://
//:CPU_LEP_CAM_GETTER_AND_PUTTER:============================://  
    
    //:PUBLIC_FILE_SECTION(UPPERCASE)://
    
        VOD A3F_CPU_LEP_GET_CAM( 
            U08     a01                //: active_0_torpid_1 ://
        ,   N4D*    n4d                //: n4d: WhereIsLevel ://
        ,   CAM* *r_cam                //: cam: LevelPackCam ://
        ){
            CAM* cam =(*r_cam);
            U08  pog =( A3M_GET );
            a3f_cpu_lep_pog_cam( pog,a01,n4d,cam );//:[ GET ]://
        }
        VOD A3F_CPU_LEP_PUT_CAM(
            U08   a01                  //: active_0_torpid_1 ://
        ,   N4D*  n4d                  //: n4d: WhereIsLevel ://
        ,   CAM*  cam                  //: cam: LevelPackCam ://
        ){
            U08  pog =( A3M_PUT );
            a3f_cpu_lep_pog_cam( pog,a01,n4d,cam );//:[ GET ]://
        }
        
        #define A3F_CPU_LEP_L80_CAM_PUT [USE:*_POG_L80_CAM]
        #define A3F_CPU_LEP_L80_PUT_CAM [USE:*_POG_L80_CAM]
        #define A3F_L80_CAM_PUT         [USE:*_POG_L80_CAM]
        
        #define A3F_CPU_LEP_L80_CAM_GET [USE:*_POG_L80_CAM]
        #define A3F_CPU_LEP_L80_GET_CAM [USE:*_POG_L80_CAM]
        #define A3F_L80_CAM_GET         [USE:*_POG_L80_CAM]
        
        #define A3F_L80_CAM_POG         [USE:*_POG_L80_CAM]
        
        #define A3F_CPU_LEP_GET_L80_CAM [USE:*_POG_L80_CAM]
        #define A3F_CPU_LEP_PUT_L80_CAM [USE:*_POG_L80_CAM]
        
        VOD A3F_CPU_LEP_POG_L80_CAM(
            U08  pog   //:Put_OR_Get
        ,   L80* l80   //:Level Index To Store In
        ,   CAM* cam   //:Camera To Get Or Set
        ){
        /** ******************************************** ***
        *** PutOrGet(POG) a camera object into levelpack ***
        *** using linear room index (L80).               ***
        *** ******************************************** **/
        #define A01 ( A3M_ACT ) //:Active_Buffer
        
            if( A3C_BUG >= 1 ){
                if( 0
                ||  l80 -> lev_dex  < (   0  )
                ||  l80 -> lev_dex  > ( 80-1 )
                ){
                    printf("\n");
                    printf("\t[l80.lev_dex]:(%d)\n"
                           , l80 -> lev_dex );;
                    printf("\n");
                    ERR("[PUT_CAM:L80_OOB]");
                };;
            };;
        
            N4D stk_n4d ={ 0 }; N4D* n4d = &( stk_n4d );
            
            A3F_CONVENG_L80_N4D( l80 , &(n4d) );
            
            a3f_cpu_lep_pog_cam( pog,A01,n4d,cam );
            
        #undef  A01
        }
    
//:============================:CPU_LEP_CAM_GETTER_AND_PUTTER://
//:PUT_GET_DEFAULT_MOS_DATA:=================================://

    VOD A3F_CPU_LEP_DMD_ONE_PUT(
        U08     a01 
    ,   I32     sid
    ,   DMD*    dmd
    ){
        assert( sid >= 0 && sid <=(16-1)   );
        assert( sid <= ( A3M_M_I_SID_MOS ) );
        
        U32 u32 =( 0 ); //:dmd as bitpacked u32
        
        A3F_DMD_U32( dmd , &(u32) );
        
        I32 vin = ( A3M_LEP_DMD_BEG + sid ); //:@VIN@://
        assert( vin >= A3M_LEP_DMD_BEG );
        assert( vin <= A3M_LEP_DMD_END );
    
        A3F_CPU_LEP_PUT_VAR(
            a01,vin,u32 );
    }
    VOD A3F_CPU_LEP_DMD_ONE_GET(
        U08     a01
    ,   I32     sid
    ,   DMD* *r_dmd
    ){
        if( A3C_BUG >= 1 ){
            if( a01 != 0 && a01 != 1 ){
                ERR("[DMD_ONE_GET:OOB_A01]");
            };;
            if( sid < 0 || sid > A3M_M_I_SID_MOS ){
                ERR("[DMD_ONE_GET:OOB_SID]");
            };;
        };;
        
        DMD* dmd =( *r_dmd );
        
        I32 vin = ( A3M_LEP_DMD_BEG + sid );
        assert( vin >= A3M_LEP_DMD_BEG );
        assert( vin <= A3M_LEP_DMD_END );
        
        U32 u32 = A3F_CPU_LEP_GET_VAR(
                          a01,vin     );;
        
        A3F_U32_DMD( u32 , &( dmd ) );
        
        assert( *r_dmd == dmd );
        (*r_dmd)=( dmd );
    }
    
    VOD A3F_CPU_LEP_DMD_ALL_PUT(
        U08     a01 
    ,   DMD**   arr  
    ){
        //: If[ Sparse Array ]THEN[ DMD** arr ]
        //: If[ Packed Array ]THEN[ DMD*  arr ]
        ERR("[ALL_PUT:#_Decide_If_Should_Be_Sparse_Or_Not_#]");
        
        DMD* dmd =((DMD*)0);
        
        //: @_SLOW_BUT_EASY_@ ://
        for( I32 sid = 0 ; sid <= (16-1) ; sid++ ){
        
            dmd = arr[ sid ];
            A3F_CPU_LEP_DMD_ONE_PUT( a01 , sid , dmd );
        };;
    }
    VOD A3F_CPU_LEP_DMD_ALL_GET(
        U08      a01                   //: Active_0_Torpid_1 ://
    ,   DMD** *r_arr                   //: Array_Of[ DMD ]   ://
    ){
        //: If[ Sparse Array ]THEN[ DMD**  *r_arr ]
        //: If[ Packed Array ]THEN[ DMD*   *r_arr ]
        ERR("[ALL_GET:#_Decide_If_Should_Be_Sparse_Or_Not_#]");
    
        DMD** arr = (*r_arr);
        DMD*  dmd = ((DMD*)0);
        
        //: @_SLOW_BUT_EASY_@ ://
        for( I32 sid = 0 ; sid <= (16-1) ; sid++ ){
        
            dmd = arr[ sid ];
            if( A3C_BUG >= 1 ){
                if( ((DMD*)0) == dmd ){
                    ERR("[@ASAHE@]");
                };;
            };;
            
            A3F_CPU_LEP_DMD_ONE_GET( a01 , sid , &(dmd) );
        };;
    }
//:=================================:PUT_GET_DEFAULT_MOS_DATA://
//:MONKEY_WRENCH_MACROS:=====================================://

    //: Decided "[put|get]" always last as is verb.      ://
    //: We also don't know ahead of time if there will   ://
    //: be more than one putter. And sometimes there     ://
    //: is only one putter type. Thus "get/put" is most  ://
    //: often the most specific qualifier.               ://
    #define A3F_LEP_PUT_DMD [USE:A3F_CPU_LEP_DMD_PUT]
    #define A3F_LEP_GET_DMD [USE:A3F_CPU_LEP_DMD_GET]

    //:Without Context, General To Specific Is "CAM" last:
    #define A3F_CPU_LEP_CAM_GET [USE:A3F_CPU_LEP_GET_CAM]
    #define A3F_CPU_LEP_CAM_PUT [USE:A3F_CPU_LEP_PUT_CAM]

    #define a3f_cpu_lep_put_var [PUBLIC_USE_ALL_CAPITALS]
    #define a3f_cpu_lep_get_var [PUBLIC_USE_ALL_CAPITALS]
    
    #define a3f_cpu_lep_put     [USE:A3F_CPU_LEP_PUT_VAR]
    #define a3f_cpu_lep_get     [USE:A3F_CPU_LEP_GET_VAR]
    #define a3f_cpu_lep_var_put [USE:A3F_CPU_LEP_PUT_VAR]
    #define a3f_cpu_lep_var_get [USE:A3F_CPU_LEP_GET_VAR]
    
    #define A33_CPU_LEP_PUT     [USE:A3F_CPU_LEP_PUT_VAR]
    #define A33_CPU_LEP_GET     [USE:A3F_CPU_LEP_GET_VAR]
    #define A33_CPU_LEP_VAR_PUT [USE:A3F_CPU_LEP_PUT_VAR]
    #define A33_CPU_LEP_VAR_GET [USE:A3F_CPU_LEP_GET_VAR]
    
    #define a3f_cpu_lep_dex_var [USE:a3f_cpu_lep_[pin|cin]_var]
    #define a3f_cpu_lep_var_dex [USE:a3f_cpu_lep_[pin|cin]_var]
    
    #define a3f_cpu_lep_gop_var [USE:lep_pog_var:(private)]
    #define a3f_cpu_lep_var_gop [USE:lep_pog_var:(private)]
    #define a3f_cpu_lep_var_pog [USE:lep_pog_var:(private)]
    #define A3F_CPU_LEP_POG_VAR [USE:lep_pog_var:(private)]
    
    #define a3f_cpu_lep_cam_bas_pog [USE:...pog_a01_bas_cam]
    
    #define A3F_CPU_LEP_MOS_DEF_GET [U:DMD_ALL_GET|DMD_ONE_GET]
    #define A3F_CPU_LEP_MOS_GET_DEF [U:DMD_ALL_GET|DMD_ONE_GET]
    #define A3F_CPU_LEP_DMD_GET     [U:DMD_ALL_GET|DMD_ONE_GET]
    #define A3F_CPU_LEP_GET_DMD     [U:DMD_ALL_GET|DMD_ONE_GET]
    #define A3F_CPU_LEP_MOS_DMD_GET [U:DMD_ALL_PUT|DMD_ONE_PUT]
    #define A3F_CPU_LEP_MOS_GET_DMD [U:DMD_ALL_PUT|DMD_ONE_PUT]
                                     
    #define A3F_CPU_LEP_MOS_DEF_PUT [U:DMD_ALL_PUT|DMD_ONE_PUT]
    #define A3F_CPU_LEP_MOS_PUT_DEF [U:DMD_ALL_PUT|DMD_ONE_PUT]
    #define A3F_CPU_LEP_DMD_PUT     [U:DMD_ALL_PUT|DMD_ONE_PUT]
    #define A3F_CPU_LEP_PUT_DMD     [U:DMD_ALL_PUT|DMD_ONE_PUT]
    #define A3F_CPU_LEP_MOS_DMD_PUT [U:DMD_ALL_PUT|DMD_ONE_PUT]
    #define A3F_CPU_LEP_MOS_PUT_DMD [U:DMD_ALL_PUT|DMD_ONE_PUT]
    
    
//:=====================================:MONKEY_WRENCH_MACROS://
//:CPU_LEP_FILE_MACROS:======================================://

    #undef  U08  
    #undef  U32  
    #undef  I32  
    #undef  VOD  
    #undef  DMD
    #undef  REC  
    #undef  ERR  
    #undef  LOG  
    #undef  N2D
    #undef  N4D  
    #undef  L80  
    #undef  CAM  

//:======================================:CPU_LEP_FILE_MACROS://
/** DO_NOT_EXTRACT_COMMENTS_FROM_FILE ********************** ***
    
    ~CPU_LEP_EXPENSIVE_VAR_PACKING~
    ~CPU_LEP_E_V~
        ========================================================
    
        |<------------------[ 512  pixels ]------------------>|
        |<------------------[  32  U32(s) ]------------------>|
        +-------+-------+-------+.....+-------+-------+-------+ 
        |  16   |  16   |  16   |     |  16   |  16   |  16   | 
        +-------+-------+-------+.....+-------+-------+-------+ 
        |< U32 >|< U32 >|< U32 >|     |< U32 >|< U32 >|< U32 >|
    
        +--==--+--==--+  +--==--+--==--+  R1  R2  R#  R31   R32
    R01 |  00  |  01  |..|  14  |  15  |  --  --  ..  ----  ----
        +--==--+--==--+  +--==--+--==--+  00  16  ..   992  1008
        +--==--+--==--+  +--==--+--==--+  01  17  ..   993  1009
    R02 |  16  |  17  |..|  30  |  31  |  02  18  ..   994  1010
        +--==--+--==--+  +--==--+--==--+  03  19  ..   995  1011
        ................................  04  20  ..   996  1012
        +--==--+--==--+  +--==--+--==--+  05  21  ..   997  1013
    R31 |  992 |  993 |..| 1006 | 1007 |  06  22  ..   998  1014
        +--==--+--==--+  +--==--+--==--+  07  23  ..   999  1015
        +--==--+--==--+  +--==--+--==--+  08  24  ..  1000  1016
    R32 | 1008 | 1009 |..| 1022 | 1023 |  09  25  ..  1001  1017
        +--==--+--==--+  +--==--+--==--+  10  26  ..  1002  1018
        ^      ^      ^  ^      ^      ^  11  27  ..  1003  1019
        |      ^      ^  ^      ^      |  12  28  ..  1004  1020
        |< U32>|< U32>|  |< U32>|< U32>|  13  29  ..  1005  1021
        |<-------[ 32 U32(S)  ]------->|  14  30  ..  1006  1022
        |<-------[ 512 Pixels ]------->|  15  31  ..  1007  1023
    R512                                  
        +--==--+--==--+  +--==--+--==--+  
        |16380 |16381 |..|16382 |16383 |  
        +--==--+--==--+  +--==--+--==--+ ----------------------+
        | The strips are 1 pixel    | Think about it... 16     |
        | thin so we actually have  | pixels per 32 bit var.   |
        | [16384]vars not[1024]vars.| ((512*512)/16)-1 == 16383|   
        +---------------------------+--------------------------+
                                       
        16384 / 1024 == 6X More Variables Than You Thought.                      
                                 
        ========================================================                       
                                 
    @pin_var@ : Pixel_INdex:of:VARiable          


    MOS_PUT / PUT_MOS : Wrong File. SEE[ A3F_CPU_PAE_MOS_PUT ]
    MOS_GET / GET_MOS : Wrong File. SEE[ A3F_CPU_PAE_MOS_GET ]
                                 
                                 
    @VIN@ : Variable Index (pixel index of variable)
            Could be local or global index depending on
            context.
            
    @_SLOW_BUT_EASY_@ :
        The code is "slow" (non optimal)
        But the code is "easy".
        
        Easy == Not Many Lines Of Code
        Easy == Less Error Prone
        
    @ASAHE@ : Array_Should_Already_Have_Elements
    
    #_Decide_If_Should_Be_Sparse_Or_Not_# :
        Decide if the "ALL_GET" and "ALL_PUT" functions
        for "DMD" should use a sparse array or not.
        
        I don't know which one it should use. So do not
        write tests for this code until we come across
        a point in which we want to call this code.
    
*** ********************** DO_NOT_EXTRACT_COMMENTS_FROM_FILE **/
                                 
                                 
                                 
                                 
                                 
                                 
                                 