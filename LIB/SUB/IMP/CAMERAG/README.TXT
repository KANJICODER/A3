//:ABOUT:CAMERAG:============================================://

    WARNING:!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!                                                    !!
    !!  CAMERAG is tightly coupled to engine and hijacks  !!
    !!  whatever it needs to run. I gaurantee CAMERAG     !!
    !!  to work when compiled as a standalone executable, !!
    !!  but execution of CAMERAG functions when running   !!
    !!  main game will lead to bad things.                !!
    !!                                                    !!
    !!  Mainly corruption of your viewports.              !!
    !!                                                    !!
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!:WARNING

    WHAT: Renders Frames To .PNG sequence or .GIF.
    
    HOW : Hijacks Game Engine, overriding VPC,VP0,VP1
          to fit it's needs.
          
    WHY : We don't have Vulkan,OpenGL,or Win32 code setup yet,
          but statistically, you are more likely to succeed
          the sooner you get something up on screen.
          
          I don't think that means "literally screen".
          It means, "The sooner you get visual feedback"
          "the more likely you are to succeed"


//:============================================:ABOUT:CAMERAG://
//:MORE_DETAILS:=============================================://

    Camera movement is achieved by interpolating between
    two tile-camera keyframes. The tile camera coordinates
    are in terms of different tile sizes.
    
    So you could say the tile cam is at:
    
        Level Within Matrix    : [ 2 ,  3 ]
        Game Tile Within Level : [ 32, 60 ]
        
    The two tile-camera keyframes are de-compressed into
    plank unit viewports and then linearly interpolated
    between.
    
    The results of that linear interpolation is the
    VP1 ( viewport offscreen data / world sampler )
    to use for rendering the world to bitmap.
    
//:=============================================:MORE_DETAILS://
        