//:FILE_MACROS:==============================================://

    #define U32 uint32_t
    #define I32  int32_t
    #define U08  uint8_t
    #define REC struct A3T_REC
    #define VOD void
    #define ERR a3f_pixback_err
    #define LOG a3f_pixback_log
    #define ASS assert

//:==============================================:FILE_MACROS://
//:PIXBACK_INIT:=============================================://

    VOD A3F_PIXBACK_KIL( VOD ){       //: Opposite_Of[ INI ] ://
    
        //:All backup memory should have been released://
        
        if( a3d_ini <= 0 ){
            ERR("[FORGOT_TO_CALL:A3F_INI()]");
        };;
        if( a3d_pixback_ini <= 0 ){
            ERR("[PIXBACK_WAS_NEVER_INITED]");
        };;
        
        I32 active_allocations =( 0 );
        I32 rin ;
        for( rin = 0 ; rin <= (16-1) ; rin++ ){
            if( ((U08*)0) != A3D_PIXBACK_PIX[ rin ] ){
                active_allocations++;
                printf("[REC:rin]:(%d)\n" , rin );
                printf("[RIN_NAM]:(%s)\n" , A3D_RIN_NAM[ rin ]);
            };;
        };;
        if( active_allocations > 0 ){
            ERR("[#_AAP_2021_#]");
        };;
    }
    VOD A3F_PIXBACK_INI( VOD ){       //: Opposite_Of[ KIL ] ://
    
        LOG("[BEG:A3F_PIXBACK_INI]");
    
        //:Init_Rectangle_Lookup_Table:----------------------://
        #define BAR_REC A3D_PIXBACK_REC 
        
        /** BAR_REC : Backup_And_Restore_RECtangle **/
        
            BAR_REC[ A3M_RIN_TAU ] = &( A3D_PIX2048_TAU );
            BAR_REC[ A3M_RIN_WOR ] = &( A3D_PIX2048_WOR );
            BAR_REC[ A3M_RIN_FEL ] = &( A3D_PIX2048_FEL );
            BAR_REC[ A3M_RIN_003 ] =  ((REC*)0) ;
                         
            BAR_REC[ A3M_RIN_T_0 ] = &( A3D_PIX2048_T_0 );
            BAR_REC[ A3M_RIN_T_1 ] = &( A3D_PIX2048_T_1 );
            BAR_REC[ A3M_RIN_006 ] =  ((REC*)0) ;
            BAR_REC[ A3M_RIN_007 ] =  ((REC*)0) ;
                         
            BAR_REC[ A3M_RIN_T_2 ] = &( A3D_PIX2048_T_2 );
            BAR_REC[ A3M_RIN_ANT ] = &( A3D_PIX2048_ANT );
            BAR_REC[ A3M_RIN_LP0 ] = &( A3D_PIX2048_LP0 );
            BAR_REC[ A3M_RIN_LP1 ] = &( A3D_PIX2048_LP1 );
                         
            BAR_REC[ A3M_RIN_PE0 ] = &( A3D_PIX2048_PE0 );
            BAR_REC[ A3M_RIN_PE1 ] = &( A3D_PIX2048_PE1 );
            BAR_REC[ A3M_RIN_SB0 ] = &( A3D_PIX2048_SB0 );
            BAR_REC[ A3M_RIN_SB1 ] = &( A3D_PIX2048_SB1 );
            
            I32 non =( 0 ); //:number_of_nils
            for( I32 i = 0 ; i <= (16-1) ; i++ ){
                if( ((REC*)0) == BAR_REC[ i ] ){ non++ ; };
            };;
            assert( 3 == non );
            
            //:Lets make sure none of pointers point to same.
            for( I32 a_a = 0 ; a_a <= (16-1) ; a_a ++ ){
            for( I32 b_b = 0 ; b_b <= (16-1) ; b_b ++ ){
            
                if( 0
                ||        a_a == b_b 
                ||  ((REC*)0) == A3D_PIXBACK_REC[ a_a ]  
                ||  ((REC*)0) == A3D_PIXBACK_REC[ b_b ]  
                ){
                    continue ;
                };;
                if( &( A3D_PIXBACK_REC[ a_a ] )
                ==  &( A3D_PIXBACK_REC[ b_b ] )
                ){
                    ERR("[SAME_ADDRESS_2021]");
                };;
            };;};;
                
        #undef  BAR_REC
        //:----------------------:Init_Rectangle_Lookup_Table://
        
        a3d_pixback_ini=( 1 );
        
        LOG("[END:A3F_PIXBACK_INI]");
    }

//:=============================================:PIXBACK_INIT://
//:PRIVATE_PIXBACK_FUNCTIONS:================================://

    VOD a3f_pixback_bor(         //:bor:backup_or_restore://
        U08 rin
    ,   U08 backup_1_restore_2   //:BACKUP_1 : RESTORE_2 ://
    ){
        LOG("[BEG:a3f_pixback_bor]");
    
////    :misc:---------------------------------------------:
        
            assert( backup_1_restore_2 <= 2 );
            assert( backup_1_restore_2 >= 1 );
            assert( rin <= (16-1) );
            REC* rec =           A3D_PIXBACK_REC[ rin ];            
            assert( ((REC*)0) !=             rec );
            
            I32 wid =(I32)( (*rec).x_1 - (*rec).x_0 + 1 );
            I32 hig =(I32)( (*rec).y_1 - (*rec).y_0 + 1 );
            I32 nop = ( wid*hig ); //:NumberOfPixels
            I32 siz = ( nop*4 ) * ((I32)sizeof(U08));
            
            U32 i_x , i_y , i_i  ; //:PixelLocations
        
////    :---------------------------------------------:misc:
////    :allocate_if_backup:-------------------------------:
        #define MEM  A3D_PIXBACK_PIX[ rin ]
        
            if(  1 == backup_1_restore_2 ){
                assert( ((U08*)0) == MEM ); 
                MEM =( calloc( 1 , (size_t)siz ) );
            };;
     
            assert( ((U08*)0) != MEM ); 
          
        #undef  MEM
////    :-------------------------------:allocate_if_backup:
////    :core_logic:---------------------------------------:
        /** ******************************************** ***
        *** loop over pixels and copy to backup memory.  ***
        *** We need to m_a_l_l_o_c here, then eventually ***
        *** use our RAMCHAN memory manager.              ***
        *** ******************************************** **/
        #define PIX A3D_PIX2048
        #define BAK A3D_PIXBACK_PIX[ rin ]
        #define MCI ((4*2048*2048)-1)
        #define   R (*rec)
        
            /** !!!! Y (WHY)Axis Must Be Outer Loop!!!! **/
        
            I32 i_p =( 0 - 1 ); //:Index Of Pixel
            I32 i_c =( 0 - 4 ); //:Index Of Component
            
            for( i_y = R.y_0 ; i_y <= R.y_1 ; i_y ++ ){
            for( i_x = R.x_0 ; i_x <= R.x_1 ; i_x ++ ){
            
                i_i = ( 4 * ( i_x + ( i_y * 2048 ) ) );
                i_p +=( 1 ); i_c +=( 4 );
                
                if( A3C_BUG >= 1 ){
                    if( i_i > MCI ){
                    
                        printf("[MCI]:%d\n" , MCI );
                        printf("[i_i]:%d\n" , i_i );
                    
                        ERR("[OOB:I_I:2048_X_2048]"); 
                    };;
                    if( i_p+0 > ((nop*1)-1) ){  
                        ERR("[OOB:I_P]"); 
                    };;
                    if( i_c+3 > ((nop*4)-1) ){
                    
                        printf("\t\n");
                        printf("\t[i_c]:(%d)\n" , i_c      );
                        printf("\t[np4]:(%d)\n" ,(nop*4)-1 );
                        printf("\t[nop]:(%d)\n" , nop      );
                        printf("\t\n");
                        
                        printf("\t[rin_nam]:(%s)\n",
                        A3D_RIN_NAM[rin]);;
                        printf("\t\n");
                        
                        ERR("[OOB:I_C]"); 
                        
                    };;
                };;
                
                //: #_MIKE_ACTON_MIGHT_HATE_ME_# ://
                switch( backup_1_restore_2 ){
                default: ERR("[1OR2]");break;
                case 1 :
                    BAK[ i_c + 0 ] = PIX[ i_i + 0 ] ;
                    BAK[ i_c + 1 ] = PIX[ i_i + 1 ] ;
                    BAK[ i_c + 2 ] = PIX[ i_i + 2 ] ;
                    BAK[ i_c + 3 ] = PIX[ i_i + 3 ] ;
                    break;
                case 2 :
                    PIX[ i_i + 0 ] = BAK[ i_c + 0 ] ;
                    PIX[ i_i + 1 ] = BAK[ i_c + 1 ] ;
                    PIX[ i_i + 2 ] = BAK[ i_c + 2 ] ;
                    PIX[ i_i + 3 ] = BAK[ i_c + 3 ] ;
                    break;
                };;
            
            };;};;
        #undef  PIX ////  <----------------PIX:Main_Pixels : 
        #undef  BAK ////  <----------------BAK:Backup_Copy : 
        #undef  MCI ////  <----------------MAX:ComponentDex:
        #undef    R ////  <----------------  R:Rectangle   :
////    :---------------------------------------:core_logic:    
////    :f_r_e_e_if_restore:-------------------------------:
        
            if( 2 == backup_1_restore_2 ){
            
                free( A3D_PIXBACK_PIX[ rin ] );
                      A3D_PIXBACK_PIX[ rin ]=((U08*)0);
            };;
////    :-------------------------------:f_r_e_e_if_restore:

    LOG("[END:a3f_pixback_bor]");
    }
//:================================:PRIVATE_PIXBACK_FUNCTIONS://
//:PIXBACK_CORE_FUNCS:=======================================://

    #define BACKUP_1  ( 1 )
    #define RESTORE_2 ( 2 )
    
        VOD A3F_PIXBACK_BAK(                       //: @BAK@ ://
            U08 rin                                //: @rin@ ://
        ){
            ASS(((U08*)0) == A3D_PIXBACK_PIX[ rin ] ); 
            a3f_pixback_bor( rin , BACKUP_1 );
            ASS(((U08*)0) != A3D_PIXBACK_PIX[ rin ] ); 
        }
        VOD A3F_PIXBACK_RES(                       //: @RES@ ://
            U08 rin                                //: @rin@ ://
        ){
            ASS(((U08*)0) != A3D_PIXBACK_PIX[ rin ] ); 
            a3f_pixback_bor( rin , RESTORE_2 );
            
            if( A3C_BUG >= 1){
            if(((U08*)0) != A3D_PIXBACK_PIX[ rin ] ){
            
                ERR("[SHOULD_HAVE_BEEN_ZEROED_OUT]");
            };;};;
        }
    #undef  BACKUP_1
    #undef  RESTORE_2
    
//:=======================================:PIXBACK_CORE_FUNCS://
//:MONKEY_WRECH_MACROS:======================================://

    //:Backup Helpers Are Just Bloat:
    
        #define A3F_PIXBACK_BAK_PAE [#_NO_BACKUP_HELPERS_#]
        #define A3F_PIXBACK_BAK_PE0 [#_NO_BACKUP_HELPERS_#]
        #define A3F_PIXBACK_BAK_PE1 [#_NO_BACKUP_HELPERS_#]
        
        #define A3F_PIXBACK_BAK_PAE [#_NO_BACKUP_HELPERS_#]
        #define A3F_PIXBACK_BAK_PE0 [#_NO_BACKUP_HELPERS_#]
        #define A3F_PIXBACK_BAK_PE1 [#_NO_BACKUP_HELPERS_#]
        
        #define A3F_PIXBACK_BAK_PAE [#_NO_BACKUP_HELPERS_#]
        #define A3F_PIXBACK_BAK_PE0 [#_NO_BACKUP_HELPERS_#]
        #define A3F_PIXBACK_BAK_PE1 [#_NO_BACKUP_HELPERS_#]
    
    //:Restore Helpers Are Just Bloat:
    
        #define A3F_PIXBACK_RES_PAE [#_NO_RESET_HELPERS_#]
        #define A3F_PIXBACK_RES_PE0 [#_NO_RESET_HELPERS_#]
        #define A3F_PIXBACK_RES_PE1 [#_NO_RESET_HELPERS_#]
        
        #define A3F_PIXBACK_RES_PAE [#_NO_RESET_HELPERS_#]
        #define A3F_PIXBACK_RES_PE0 [#_NO_RESET_HELPERS_#]
        #define A3F_PIXBACK_RES_PE1 [#_NO_RESET_HELPERS_#]
        
        #define A3F_PIXBACK_RES_PAE [#_NO_RESET_HELPERS_#]
        #define A3F_PIXBACK_RES_PE0 [#_NO_RESET_HELPERS_#]
        #define A3F_PIXBACK_RES_PE1 [#_NO_RESET_HELPERS_#]
        
        //:------------------------------------------://
        #define A3F_PIXBACK_LEP \
        USE[A3F_PIXBACK_[BAK|RES](A3M_RIN_[LP0|LP1])]
        
        #define A3F_PIXBACK_PAE \
        USE[A3F_PIXBACK_[BAK|RES](A3M_RIN_[PE0|PE1])]
        
        #define A3F_PIXBACK_SAB \
        USE[A3F_PIXBACK_[BAK|RES](A3M_RIN_[SB0|SB1])]
        //:------------------------------------------://
        #define A3F_PIXBACK_LP0 \
        USE[A3F_PIXBACK_[BAK|RES](A3M_RIN_LP1)]
        
        #define A3F_PIXBACK_PE0 \
        USE[A3F_PIXBACK_[BAK|RES](A3M_RIN_PE1)]
        
        #define A3F_PIXBACK_SB0 \
        USE[A3F_PIXBACK_[BAK|RES](A3M_RIN_SB1)]
        //:------------------------------------------://
        #define A3F_PIXBACK_LP1 \
        USE[A3F_PIXBACK_[BAK|RES](A3M_RIN_LP1)]
        
        #define A3F_PIXBACK_PE1 \
        USE[A3F_PIXBACK_[BAK|RES](A3M_RIN_PE1)]
        
        #define A3F_PIXBACK_SB1 \
        USE[A3F_PIXBACK_[BAK|RES](A3M_RIN_SB1)]
        //:------------------------------------------://
        
    //:Wrong_Rectangle_Names:--------------------------------://
    
        /////////////////////////////////////////////////////
        ///  [PIXBACK] just borrows [PIX2048] rectangles. ///
        ///  So ownership of the update rectangles in     ///
        ///  [PIXBACK] is [PIX2048]                       ///
        /////////////////////////////////////////////////////
    
        #define A3D_PIXBACK_TAU    [OWNER_IS:A3D_PIX2048_TAU]
        #define A3D_PIXBACK_WOR    [OWNER_IS:A3D_PIX2048_WOR]
        #define A3D_PIXBACK_FEL    [OWNER_IS:A3D_PIX2048_FEL]
                                   
        #define A3D_PIXBACK_T_0    [OWNER_IS:A3D_PIX2048_T_0]
        #define A3D_PIXBACK_T_1    [OWNER_IS:A3D_PIX2048_T_1]
                                    
        #define A3D_PIXBACK_T_2    [OWNER_IS:A3D_PIX2048_T_2]
        #define A3D_PIXBACK_ANT    [OWNER_IS:A3D_PIX2048_ANT]
        #define A3D_PIXBACK_LP0    [OWNER_IS:A3D_PIX2048_LP0]
        #define A3D_PIXBACK_LP1    [OWNER_IS:A3D_PIX2048_LP1]
                                   
        #define A3D_PIXBACK_PE0    [OWNER_IS:A3D_PIX2048_PE0]
        #define A3D_PIXBACK_PE1    [OWNER_IS:A3D_PIX2048_PE1]
        #define A3D_PIXBACK_SB0    [OWNER_IS:A3D_PIX2048_SB0]
        #define A3D_PIXBACK_SB1    [OWNER_IS:A3D_PIX2048_SB1]
        
    //:--------------------------------:Wrong_Rectangle_Names://
        
//:======================================:MONKEY_WRECH_MACROS://
//:FILE_MACROS:==============================================://

    #undef  U32
    #undef  I32  
    #undef  U08  
    #undef  REC  
    #undef  VOD  
    #undef  ERR  
    #undef  LOG
    #undef  ASS

//:==============================================:FILE_MACROS://
/** DO_NOT_REMOVE_COMMENTS_FROM_FILE *********************** ***

    #rin# / ~rin~ : Rectangle_INdex
    
        rin:0 ==> TAU rectangle
        rin:1 ==> WOR rectangle
        rin:2 ==> FEL rectangle
        rin:3 ==> ALWAYS NULL BECAUSE[ FEL ]IS[ 2_x_2 ]
        rin:6 ==> ALWAYS NULL BECAUSE[ FEL ]IS[ 2_x_2 ]
        rin:7 ==> ALWAYS NULL BECAUSE[ FEL ]IS[ 2_x_2 ]
        rin:# ==> Some sector of PIX2048 buffer.
        
        +---+---+---+---+   +---+---+---+---+
        | 0 | 1 | 2   3 |   |TAU|WOR|       |
        +---+---+   +   +   +---+---+  FEL  +
        | 4 | 5 | 6   7 |   |T_0|T_1|       |
        +---+---+---+---+   +---+---+---+---+
        | 8 | 9 |10 |11 |   |T_2|ANT|LP0|LP1|
        +---+---+---+---+   +---+---+---+---+
        |12 |13 |14 |15 |   |PE0|PE1|SB0|SB1|
        +---+---+---+---+   +---+---+---+---+
        
    #_NO_HELPER_SEE_ONE_LINER_#: SEE[ TRASH/004._ ]
    #_NO_RESET_HELPERS_#       : SEE[ TRASH/004._ ]
    #_NO_BACKUP_HELPERS_#      : SEE[ TRASH/004._ ]

    #_AAP_2021_# :
        AAP : Active_Allocation_Present
        When shutting down the engine, the backup allocations
        should already be freed. Backups are designed for
        use with unit tests, which happen on startup.
        
    #_MIKE_ACTON_MIGHT_HATE_ME_#
        TYPO[ #_MIKE_ACTION_MIGHT_HATE_ME_# ]
        /// ************************************ ///
        /// Mike Acton would probably not like   ///
        /// this. But we are writing the code    ///
        /// in a way that should make it less    ///
        /// likely that data is read in a        ///
        /// different order than it is written.  ///
        /// ************************************ ///
        
    @rin@ : Rectangle_INdex 
            Basically an enumeration identifying
            different rectangles in our backup.
            SEE[ #rin# ]
            
    @BAK@ : Backup
    @RES@ : Restore
    @BOR@ : Backup_Or_Restore
    
    @BAR_REC@ : Backu_And_Restore RECtangle

*** *********************** DO_NOT_REMOVE_COMMENTS_FROM_FILE **/