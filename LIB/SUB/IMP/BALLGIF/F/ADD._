    void A3F_BALLGIF_ADD(
        U08*         img  //: Image  To Add To[ gif ] ://
    ,   I32          wid  //: Width  In Pixels        ://
    ,   I32          hig  //: Height In Pixels        ://
    ,   I32          dex  //: Frame  Index Adding To  ://
    ,   GIF*         gif  //: Animated GIF Container  ://
    ){
        a3f_ballgif_add_eci(img,wid,hig,dex,gif);
        
        U08 r_r , g_g , b_b , a_a ; //:RGBA
        I32 i_x , i_y , i_4 , i_1 ; //:COORDS
        U08 key ; 
        I32 m_4 ; //:Max Index Of [ i_4 ]
        I32 m_1 ; //:Max Index Of [ i_1 ]
        
        m_4 =( ( wid * hig * 4 ) - 3 ); 
        m_1 =( ( wid * hig * 1 ) - 0 );
        
        //: #_TEMPORAL_LOCALITY_OF_SCANLINES_# ://
        
        I08 skip_key     ;
        I08 phase        ;
        I08 passes       ;
        I08 pass_counter ;
        I08 interlace_style=( a3d_ballgif_int );  
        
        if( 1 == interlace_style ){
            //:One Pass, #_CRT_SCANLINES_# , #_Y_OUTER_LOOP_#
            phase  = 1 ;
            passes = 1 ;
        }else
        if( 2 == interlace_style ){
            //:One Pass, #_NEO_SCANLINES_# , #_X_OUTER_LOOP_#
            phase  = 2 ;
            passes = 1 ;
        }else
        if( 3 == interlace_style ){
            //:WAVY_INTERLACE_PATTERN
            //:Interlace both scanlines using checker mask.
            phase  = 1 ;
            passes = 2 ;
        }else
        if( 4 == interlace_style ){
            //:DIAMOND_INTERLACE_PATTERN
            //:Same as style 3, but do not use CONTINUE.
            //:More processing, but will be a more accurate
            //:blend of interlace styles 1 & 2 together.
            phase = 1 ;
            passes= 2 ;
        }else{
            ERR("[UNKNOWN_INTERLACE_STYLE]");
        };;
        
        pass_counter=( 0 );
        for( phase = phase ; phase <= 2 ; phase ++ ){
        
                pass_counter++;
            if( pass_counter > passes ){ break; };
        
        for( I32 i_i = 0 ; i_i <= ((wid*hig)-1) ; i_i ++ ){
        
            skip_key=( 0 );
        
            //:Least convoluted way to allow swapping 
            //:of x&y nested parts of loop.
            //:This also enables checkerboard blending.
            if( 1 == phase ){          //: #_CRT_SCANLINES_# ://
                i_y =( i_i / ( wid )       );
                i_x =( i_i - ( wid * i_y ) );
                if( i_y % 2 != i_x % 2 ){ skip_key = 1; };
            }else
            if( 2 == phase ){          //: #_NEO_SCANLINES_# ://
                i_x =( i_i / ( hig )       );
                i_y =( i_i - ( hig * i_x ) );
                if( i_y % 2 == i_x % 2 ){ skip_key = 1; };
            };;
            
            //:4_and_3_Skip_Keys:----------------------------://
            
                if( skip_key >= 1 && 4 == interlace_style ){
                    //:Do_Nothing
                }else
                if( skip_key >= 1 && 3 == interlace_style ){
                    continue;
                }else
                if( skip_key >= 1 && 2 == interlace_style ){
                    skip_key =( 0 );
                }else
                if( skip_key >= 1 && 1 == interlace_style ){
                    skip_key =( 0 );
                };;
                
            //:----------------------------:4_and_3_Skip_Keys://
        
            //:Component Indexes for[ img ]and[ gif ]:
            i_4 = ( 4 * ( i_x + ( i_y * wid ) ) ); //:<--[ img ]
            i_1 = ( 1 * ( i_x + ( i_y * wid ) ) ); //:<--[ gif ]
            
            //:bounds_check_indexes:
            if( i_4 > m_4 ){ ERR("[BALLGIF_ADD:OOB:m_4]"); };
            if( i_1 > m_1 ){ ERR("[BALLGIF_ADD:OOB:m_1]"); };
        
            //:Lookup RGBA pixel within img:
            r_r = img[ i_4 + 0 ];
            g_g = img[ i_4 + 1 ];
            b_b = img[ i_4 + 2 ];
            a_a = img[ i_4 + 3 ];
            
            //:Get Pallet Key Using RGBA pixel color:
            key = a3f_ballgif_rgb_key( 
                    r_r , g_g , b_b , a_a );
                    
            //:-------------------------------------://
            //:[TODO]: Optimize this for interlace  ://
            //:        style 4 so we are not just   ://
            //:        throwing away calculations.  ://
            //:-------------------------------------://
            if( skip_key <= 0 ){
            
                //:Put Pallet Key Into Gif FrameBuffer:
                gif -> gifwrap -> frame[ i_1 ]=( key );
            
            };;
        };;};;
        
        //:Officially add filled frame gif:
        //:P_F("[about_to_officially_add_frame]\n");
        ge_add_frame(
                 ( gif -> gifwrap )
        ,   (U16)( gif -> gif_tim )
        );;
    }
    
/** ******************************************************** ***
*** COMMENTS_ARE_READ_LAST_OR_NEVER ************************ ***
*** ******************************************************** ***

    #_CRT_SCANLINES_#
    
        Scanlines going left-to-right , THEN top-to-bottom
        like a CRT monitor. (CRT==Cathode_Ray_Tube)
        
    #_NEO_SCANLINES_#
    
        Scanlines going top-top-bottom , THEN right-to-left
        like the movie "THE MATRIX 1999".
        "NEO" because "NEO" is name of main character in movie.
        
    #_TEMPORAL_LOCALITY_OF_SCANLINES_#
        //:--------------------------------------------------://
        //: The order of these loops will affect how your    ://
        //: interlacing looks because pixels either have     ://
        //: temporal locality horizontally or vertically,    ://
        //: but not both. We could finess stippling by       ://
        //: rendering a [ y,x ] loop then [ x,y ] loop       ://
        //: and then average them together by using          ://
        //: a checkerboard mask to approximate a             ://
        //: 50/50 blend of both.                             ://
        //:--------------------------------------------------://
        
        #_Y_OUTER_LOOP_#:
        ( i_y = 0 ; i_y <= (hig-1) ; i_y ++ ){
        ( i_x = 0 ; i_x <= (wid-1) ; i_x ++ ){  ...  }};;
        
        #_X_OUTER_LOOP_#:
        ( i_x = 0 ; i_x <= (wid-1) ; i_x ++ ){
        ( i_y = 0 ; i_y <= (hig-1) ; i_y ++ ){  ...  }};;

        
*** ******************************************************** ***
*** ************************ COMMENTS_ARE_READ_LAST_OR_NEVER ***
*** ******************************************************** **/