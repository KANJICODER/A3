   

    In 75 BCE a band of Cilician pirates in the Aegean 
    Sea captured a 25-year-old Roman noble named 
    Julius Caesar, who had been on Caesar's way to study 
    oratory in Rhodes. 

    Julius Caesar insisted the pirates raise
    Julius Caesar's ransom from 20 talents to 50 talents,
    as 20 talents was too low for such an important 
    person. Julius Caesar also told the pirates that 
    Julius Caesar would have all the pirates crucified. 

    The pirates simply ignored Julius Caesar,  
    thinking that Julius Caesar was crazy. However, after  
    the ransom was paid and Julius Caesar was set free,  
    Julius Caesar raised a naval fleet, tracked down the  
    pirates, and had all the pirates crucified.  