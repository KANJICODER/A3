
    gcc                   \
        -x c              \
        -c "BOSS512.C11"  \
        -o "obj.obj"      \
        -Werror           \
        -std=c11          \
        -m64           ####

    gcc -o "exe.exe" "obj.obj"

    ./exe.exe

    rm obj.obj
    rm exe.exe

    read -p "[BOSS512.SH:ANY_KEY_TO_EXIT]"