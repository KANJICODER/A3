
    void
    a3f_boss512_zoo(
        DOU* dou
    ,   F32  zoo //:[ zoomper : 0.0 == NONE. 1.0 == MAX ] ://
    ){
        assert( zoo >= 0.0 && zoo <= 1.0 );
    
        U08* src ; //:Source Buffer
        U08* dst ; //:Destination Buffer
        
        I32  wid =( dou -> wid );
        I32  hig =( dou -> hig );
        
        ///  w_1 =( 0 ); //:Width Of Zoom Region. NOT NEEDED
        
        I32  i_x     ; //: PIXEL_LOCATIONS
        I32  i_y     ; //: PIXEL_LOCATIONS
        ///  i_i     ; //: PIXEL_LOCATIONS
        I32  i_1     ; //: SOURCE PIXEL INDEX (src's i_i)
        I32  i_0     ; //: DEST   PIXEL INDEX (dst's i_i)
        
    //| REC  r_s ={0}; //: rec_src : INCLUSIVE_RECTANGLE |//
        REC  r_d ={0}; //: rec_dst : INCLUSIVE_RECTANGLE ://
        REC  mid ={0}; //: MIDDLE OF THE SOURCE CANVAS   ://
             
        I32  m_x =(0); //:Max delta X
        I32  m_y =(0); //:Max delta Y
             
        I32  c_x =(0); //:Current delta X
        I32  c_y =(0); //:Current delta Y
        
        F32  p_x , p_y ; //: Percentage X & Y locations.
        I32  z_x , z_y ; //: Zoomed X & Y Pixel Coord.
        
        //: r_s is never used directly. But conceptually
        //: this rectangle obect exists. Kept in the code
        //: for reference. DATE[ 2021_12_26 ]

    //+--------------------------------+//
    //| r_s.x_0 = 0;                   |//
    //| r_s.y_0 = 0;                   |//
    //| r_s.x_1 = ( dou -> wid - 1 );  |//
    //| r_s.y_1 = ( dou -> hig - 1 );  |//
    //+--------------------------------+//
        
        //:Because even pixel images have no center pixel,
        //:the middle is described as a 2x2 rectangle.
        mid.x_0 =( (wid-1) / 2 );
        mid.x_1 =( (wid+0) / 2 );
        mid.y_0 =( (hig-1) / 2 );
        mid.y_1 =( (hig+0) / 2 );
        
        //:The most of which you can add or subtract
        //:from the center without going out of image bounds:
        m_x =( (wid/2) - 1 );
        m_y =( (hig/2) - 1 );
        
        //:Current amount to add or subtract from center to
        //:create a zoomed in viewport:
        //:If zoom is ZERO we should subtract the max amount
        //:to create a subselection area that matches the size
        //:of the original image, effectively not zooming at 
        //:all.
        c_x =(I32)( (1.0 - zoo)*((F32)(m_x)));
        c_y =(I32)( (1.0 - zoo)*((F32)(m_y)));
        
        r_d.x_0 = mid.x_0 - c_x;
        r_d.x_1 = mid.x_1 + c_x;
        r_d.y_0 = mid.y_0 - c_y;
        r_d.y_1 = mid.y_1 + c_y;
        
        //:Get source and dest buffers:
        src = A3F_DOUBLEP_GET_A_1_SRC( dou );
        dst = A3F_DOUBLEP_GET_A_0_DST( dou );
        
        //:WRONG: Get width of zoomed in sub-frame:
        //: w_1 =( r_d.x_1 - r_d.x_0 + 1 );
        
        //:Nearest neighbor zoom in:
        for( i_y = 0 ; i_y <= ( hig - 1 ) ; i_y ++ ){
        for( i_x = 0 ; i_x <= ( wid - 1 ) ; i_x ++ ){
        
            //:Percentage Coordiate Of Dest Pixel:
            p_x = ( ((F32)i_x) / ((F32)(wid-1)) );
            p_y = ( ((F32)i_y) / ((F32)(hig-1)) );
            
            //:Actual coordinate within zoom frame:
            //:No +1 because canceled out by -1 in formula.
            #define I (I32) //:I32 CAST
            #define F (F32) //:F32 CAST

                z_x = r_d.x_0 + I( p_x * F(r_d.x_1 - r_d.x_0) );
                z_y = r_d.y_0 + I( p_y * F(r_d.y_1 - r_d.y_0) );
            
            #undef  I
            #undef  F
            //:Figure out mapping between pixel indexes:
            //:Both use SAME[ wid ]because the zoomed in
            //:sub-frame is still part of origina image.
            i_1 =( 4 * ( z_x + ( z_y * wid ) ) ); //:SRC  PIXEL
            i_0 =( 4 * ( i_x + ( i_y * wid ) ) ); //:DEST PIXEL
            
            //:Copy Pixels:
            dst[ i_0 + 0 ]=src[ i_1 + 0 ];
            dst[ i_0 + 1 ]=src[ i_1 + 1 ];
            dst[ i_0 + 2 ]=src[ i_1 + 2 ];
            dst[ i_0 + 3 ]=src[ i_1 + 3 ];
        
        };;};;
        
        //:BUFFER SWAP: (dst becomes the new src)
        A3F_DOUBLEP_SWA_YES( dou );
    
    }