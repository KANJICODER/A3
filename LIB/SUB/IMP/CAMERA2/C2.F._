//:CAMERA2_FILE_MACROS:======================================://

    #define CHR const char
    #define I08   int8_t
    #define U08  uint8_t
    #define U32 uint32_t
    #define I32  int32_t
    #define F64  double
    #define IV2 struct A3T_IV2
    #define REC struct A3T_REC
    #define CAM struct A3T_CAM
    #define MOS struct A3T_MOS
    #define L80 struct A3T_L80
    #define N2D struct A3T_N2D
    #define CA2 struct A3T_CAMERA2_CAM
    #define VOD void
    #define LOG a3f_camera2_log
    #define ERR a3f_camera2_err
    
//:======================================:CAMERA2_FILE_MACROS://
//:KEEP_ON_TICKING:==========================================://

    I08 A3F_CAMERA2_KOT( VOD ){
    
        return( a3d_camera2_kot );
        
    }
//:==========================================:KEEP_ON_TICKING://
//:PNG_BITMAP_ALLOCATION_AND_DEALLOCATION:===================://

    VOD a3f_camera2_pix_ini(){
    
        if( a3d_camera2_pix_has >= 1 ){
            ERR("[@AINI@:CAMERA2]");
        }else{
            a3d_camera2_pix_has  =(1);
            A3F_RAMCHAN_PIX_MAK( 
                a3d_camera2_pix_wid 
            ,   a3d_camera2_pix_hig 
            , &(a3d_camera2_pix_arr)
            );;
        };;
    }
    VOD a3f_camera2_pix_kil(){
    
        if( a3d_camera2_pix_has <= 0 ){
            ERR("[@PANC@:CAMERA2]");
        }else{
            a3d_camera2_pix_has  =(0);
            A3F_RAMCHAN_PIX_DEL( 
            &(a3d_camera2_pix_arr)
            , "[a3d_camera2_pix_arr:CAMERA2]"
            );;
        };;
    }
//:===================:PNG_BITMAP_ALLOCATION_AND_DEALLOCATION://
//:KIL_AND_OFF:==============================================://

    VOD A3F_CAMERA2_KIL( VOD ){
    
        /**Destructor/Killer. Shutting Down.**/
        
        if( a3d_camera2_kot >= 1 ){            //:RUNNING://
        
            printf("[CAMERA2_WARNING:@SDWR@]\n");
        
            if( a3d_camera2_pix_has >= 1 ){
                a3f_camera2_pix_kil();
            };;
        }else
        if( a3d_camera2_kot <= 0 ){            //:STOPPED://
        
            if( a3d_camera2_pix_has >= 1 ){ 
                ERR("[Should_Already_Be_Deleted]"); 
            };;
        };;
    }
    
    VOD A3F_CAMERA2_OFF( VOD ){
        a3d_camera2_kot=( 0 );
        a3f_camera2_pix_kil();
    }
//:==============================================:KIL_AND_OFF://
//:CAMERA2_FUNCTIONS:========================================://

    VOD a3f_camera2_pop(
        I32 sid                       //: Sprite_ID          ://
    ,   I32 f_0                       //: Frames:Follow      ://
    ,   I32 f_1                       //: Frames:Transition  ://
    ){
    
        if( A3C_BUG >= 1 ){
            if( A3D_CAMERA2_LEN >= A3M_CAMERA2_CAP ){
               
                if( A3D_CAMERA2_LEN == A3M_CAMERA2_CAP ){
                    printf("[ABIT_OOB]\n");
                }else{
                    printf("[MUCH_OOB]\n");
                };;
                ERR("[C2_POP:OOR]"); 
            };;
        };;
    
        CA2* ca2 = &(A3D_CAMERA2_ARR[ A3D_CAMERA2_LEN++ ]);
        ca2 -> sid=( sid );
        ca2 -> f_0=( f_0 );
        ca2 -> f_1=( f_1 );
    }
    
    
    VOD a3f_camera2_rec( 
        I32     sid  //:SpriteID we want camera rectangle for.
    ,   REC* *r_rec  //:Sprite Rectangle To Return
    ){
    /** ************************************************ ***
    *** Compute Camera Rectangle Around Sprite In Level  ***
    *** DETAILED_NOTED[ #_FUNC_NOTES_CAMERA2_REC_#       ***
    *** ************************************************ **/
    
        //:STEP_001: Declare Variables ;
        
            REC* rec;
            
            CAM  stk_cam ={ 0 }; CAM* cam=&( stk_cam );
            MOS  stk_mos ={ 0 }; MOS* mos=&( stk_mos );
            N2D  stk_n2d ={ 0 }; N2D* n2d=&( stk_n2d );
            L80  stk_l80 ={ 0 }; L80* l80=&( stk_l80 );
            
            I32 a_x ; //: Active sprite position within
            I32 a_y ; //: cross measured in plank units
            
            I32 x_0 ; //: Top Left Of Sprite, in planks.
            I32 y_0 ; //:
            
            I32 x_1 ; //: Bot Left Of Sprite, in planks.
            I32 y_1 ;  
            
            /// I32 wid ; //:Width and Height Of Sprite
            /// I32 hig ; //:Measured In Tile Units
            ///           //:Maybe in future you can create
            ///           //:a hitbox [wid|hig] or an inset
            ///           //:amount for collision. But keep
            ///           //:it simple for now.
    
        //:STEP_002: Extract Reference:
        
            rec = (*r_rec);
    
        //:STEP_003: Get Sprite Position
        
            U08 a01 = ((U08)A3M_ACT);
            A3F_CPU_PAE_MOS_GET( a01 , sid , &( mos ) );
            assert( stk_mos.sid == sid );
            a_x = ( stk_mos.a_x ); //:<--a_x in Plank Units
            a_y = ( stk_mos.a_y ); //:<--a_y in plank units
            
            /// wid = ( stk_mos.wid );
            /// hig = ( stk_mos.hig );
            
            assert( a_x >= 0 && a_x <=(A3M_PUN_CRO-1) );
            assert( a_y >= 0 && a_y <=(A3M_PUN_CRO-1) );
            
        //:STEP_004: What [Level/Room] Is Sprite In?
        
            //:Divide plank unit position of sprite by
            //:the number of plank units in a tile to find
            //:the global tile position of the sprite 
            //:within the levelpack.
            stk_n2d.tx_0768 =( a_x / A3M_T_0_PUN );
            stk_n2d.ty_0768 =( a_y / A3M_T_0_PUN );
            
            if( A3C_BUG >= 1 ){
                I32 m_i = A3F_N2D_M_I( &(stk_n2d) );
                if( m_i < 0 || m_i > (5-1) ){
                    A3F_N2D_LOG( &(stk_n2d) , "[n2d]" );
                    printf("[#_FTL_DEF_MOS_POS_#?]\n");
                    ERR("[Sprite_Is_Not_Within_Room]");
                };;
            };;
            
            //:Convert linear n2d to l80
            A3F_CONVENG_N2D_L80( n2d , &( l80 ) );
            
        //:STEP_005: Get Camera Settings For This Level
            #define POG (  (U08)A3M_GET  )  
            
                if( A3C_BUG >= 1 ){
                    if( 0
                    ||  l80 -> lev_dex < 0
                    ||  l80 -> lev_dex >(80-1)
                    ){
                    #define P printf
                        P("\n");
                        P("\t[lev_dex]:(%d)\n",l80->lev_dex);
                        ERR("[OOB_CAM_LEV_DEX]");
                    #undef  P 
                    };;
                };;
                A3F_CPU_LEP_POG_L80_CAM(POG,l80,cam);
            
            #undef  POG
            
        //:STEP_006: Using Sprite Position And Camera Settings,
        //:          calculate the camera rectangle.
        
            switch( cam -> cam_typ ){ case

            A3M_LEP_CAM_VOD :
                {
                    
                    
                    //:True Origin Of Sprite Is A Selection:
                    x_0 =((a_x)-0);
                    y_0 =((a_y)-0);
                    x_1 =((a_x)+1);
                    y_1 =((a_y)+1);
                    
                    #define T_P ( A3M_T_0_PUN )
                        //:Radius in plank units around sprite:
                        //:Creates a 3_x_3 tile camera rect:
                        I32 p_w = ( T_P +(T_P/2) );    //:1.5://
                        I32 p_h = ( T_P +(T_P/2) );    //:1.5://
                    #undef  T_P
                    
                    //: +1 and -1 to make pixel perfect.
                    //: If[ p_w == 1 ), we wouldn't move
                    //: anywhere, right? (p_w==0 invalid)
                    I32 rec_x_0 =( x_0 - p_w ) + 1;
                    I32 rec_x_1 =( x_1 + p_w ) - 1;
                    I32 rec_y_0 =( y_0 - p_h ) + 1;
                    I32 rec_y_1 =( y_1 + p_h ) - 1;
                    
                    //:Should Never Go Negative!
                    //:I can imagine scenarios where
                    //:this will happen... Don't worry
                    //:about it for now.
                    assert( rec_x_0 >= 0 );
                    assert( rec_x_1 >= 0 );
                    assert( rec_y_0 >= 0 );
                    assert( rec_y_1 >= 0 );
                    
                    //:Give 1 tile boarder around sprite
                    //:for this camera RECTANGLE.
                    rec -> x_0 =(U32)( rec_x_0 );
                    rec -> x_1 =(U32)( rec_x_1 );
                    rec -> y_0 =(U32)( rec_y_0 );
                    rec -> y_1 =(U32)( rec_y_1 );
                   
                };break;case 
            A3M_LEP_CAM_MOV :
                {
                
                    ERR("[TODO:ADD_SUPPORT:A3M_LEP_CAM_MOV]");
            
                };break;case
            A3M_LEP_CAM_FIX :
                {
                
                ERR("[TODO:ADD_SUPPORT:A3M_LEP_CAM_FIX]");
            
                };break;
            default:{
                ERR("[OHNO123]");
            };;};;
            
        //:STEP_007: Return Camera Rectangle
        
            (*r_rec)=( rec );
    }
    
    U32 a3f_camera2_int_i_0_i_1_lfi_lft(
        U32 i_0  //:Starting Value
    ,   U32 i_1  //:Ending   Value
    ,   I32 lfi  //:Current Frame Index On Range  @lfi@
    ,   I32 lft  //:Total   Frames      In Range  @lft@
    ){
    #define RRU a3f_conveng_rec_rec_u32  
    
        /// ************************************* ///
        /// TODO: rec_rec_u32 is kind of a lie... ///
        ///  FIX: because the two inputs are      ///
        ///       not rectangles.                 ///
        /// ************************************* ///
        
        F64 B = ((F64)lfi) / ((F64)( lft - 1 ));
        F64 A = ( ((F64)1) - B );
        
        return(  RRU( i_0 , i_1 , A , B ) );
    
    #undef  RRU
    }
    
    VOD a3f_camera2_int(       //:int:Interpolate            ://
        REC*     r_0           //: @R_0_REC@                 ://
    ,   REC*     r_1           //: @R_1_REC@                 ://
    ,   I32      lfi           //: @lfi@                     ://
    ,   I32      lft           //: @lft@                     ://
    ,   REC*  *r_rec           //:<--Output Interp Rectangle ://
    ){
    #define         O_O (*rec) //:"libloaderapi.h" defs "OUT"://
    #define         R_0 (*r_0)
    #define         R_1 (*r_1)
    #define INTERPOLATE a3f_camera2_int_i_0_i_1_lfi_lft
    
        REC* rec = (*r_rec);
        O_O.x_0 = INTERPOLATE( R_0.x_0,R_1.x_0 , lfi,lft );
        O_O.x_1 = INTERPOLATE( R_0.x_1,R_1.x_1 , lfi,lft );
        O_O.y_0 = INTERPOLATE( R_0.y_0,R_1.y_0 , lfi,lft );
        O_O.y_1 = INTERPOLATE( R_0.y_1,R_1.y_1 , lfi,lft );
        (*r_rec)=( rec );
        
    #undef          O_O        //:"libloaderapi.h" defs "OUT"://
    #undef          R_0
    #undef          R_1
    #undef  INTERPOLATE
    } 
    
    
    VOD a3f_camera2_mov(
        I32 s_0                    //: <-- c_0.sid ( FROM )  ://
    ,   I32 s_1                    //: <-- c_1.sid (   TO )  ://
    ,   I32 lfi                    //: <-- Local Frame Index ://
    ,   I32 lft                    //: <-- Local Frame Total ://
    ){
    
        //:#_GET_BOTH_SPRITES_CAMERA_RECTANGLES_#://
        REC* r_0 = &( a3d_camera2_r_0 );
        REC* r_1 = &( a3d_camera2_r_1 );
        a3f_camera2_rec( s_0 , &(r_0) );  //:FirstSprite[SID]://
        a3f_camera2_rec( s_1 , &(r_1) );  //:Next_Sprite[SID]://
        
        //:Interpolate between rectangles:
        REC* rec =( &(a3d_camera2_rec) );
        a3f_camera2_int( r_0,r_1,lfi,lft, &(rec) );
        assert( rec == &(a3d_camera2_rec) );
        
        //:Update Stored Camera Rectangle On Taumast:
        //:If we had a [ a3f_camera2_vp1_gpu ]
        //:Right here would be it.
        A3F_TAUMAST_PUT( A3M_TAU_VP1_X_0 , rec -> x_0 );
        A3F_TAUMAST_PUT( A3M_TAU_VP1_X_1 , rec -> x_1 );
        A3F_TAUMAST_PUT( A3M_TAU_VP1_Y_0 , rec -> y_0 );
        A3F_TAUMAST_PUT( A3M_TAU_VP1_Y_1 , rec -> y_1 );
    }
    
//:========================================:CAMERA2_FUNCTIONS://
//:RENDER_AND_SAVE_IMAGE:====================================://
//:                                                          ://
//:     RENDER : A3F_CAMERA2_VP1_VP0                         ://
//:     SAVE   : A3F_CAMERA2_IMG_SAV                         ://
//:                                                          ://
//:RENDER_AND_SAVE_IMAGE:====================================://
#define IMG_DEX a3d_camera2_img
    
    VOD A3F_CAMERA2_VP1_VP0( ){  
        
        //: #_ABOUT_CAMERA2_VP1_VP0_# ://
        
        IV2 fic ={0}; //: fragment_integer_coord     ://
        U32 pix ;     //: output_pixel_from_renderer ://
        I32 p_i ;     //: pixel index of pixel.      ://
        
        //:loop_over_bitmap:---------------------------------://
        #define WID a3d_camera2_pix_wid  
        #define HIG a3d_camera2_pix_hig  
        #define ARR a3d_camera2_pix_arr  
        //:loop_over_bitmap:---------------------------------://
        
            for( fic.y = 0 ; fic.y <=( HIG - 1 ) ; fic.y ++ ){
            for( fic.x = 0 ; fic.x <=( WID - 1 ) ; fic.x ++ ){  
            
                //:Render_Pixel:
            
                    pix = A3F_VODFRAG_MEP( fic ); 
                
                //:Save Pixel To Bitmap:
                
                    p_i =( 4 * ( fic.x + ( fic.y * WID ) ) );
                    ARR[ p_i + 0 ] =(U08)(( pix >> 24 )&0xFF);
                    ARR[ p_i + 1 ] =(U08)(( pix >> 16 )&0xFF);
                    ARR[ p_i + 2 ] =(U08)(( pix >>  8 )&0xFF);
                    ARR[ p_i + 3 ] =(U08)(( pix >>  0 )&0xFF);
            };;};;
        //:---------------------------------:loop_over_bitmap://
        #undef  WID
        #undef  HIG
        #undef  ARR
        //:---------------------------------:loop_over_bitmap://
    }

    VOD A3F_CAMERA2_IMG_SAV( VOD ){
    
        //: #_WHY_IMG_SAV_# ://
        
        CHR* bas_fol = "./MOD/"  ;               //:@bas_fol@://
        CHR* bas_nam = "CA2_"    ;               //:@bas_nam@://
        A3F_FILEBOI_SAVFILE_PNG_DEX(
            bas_fol,bas_nam,IMG_DEX   
            
        ,   a3d_camera2_pix_arr                  //:@PIX_ARR@://
        ,   a3d_camera2_pix_wid                  //:@PIX_WID@:// 
        ,   a3d_camera2_pix_hig                  //:@PIX_HIG@://  
        );;
    }

#undef  IMG_DEX
//:====================================:RENDER_AND_SAVE_IMAGE://
//:CAMERA2_FUNCTIONS:========================================://

    VOD A3F_CAMERA2_TIK( VOD ){
    
        if( a3d_camera2_kot <= 0 ){
            ERR("[SHOULDNOTBETICKINGNOW]");
        };;
        
        //:Get_Point_On_Path:----------------------------://
        
            //: A3D_CAMERA2_DEX : UPDATED AT END OF FUNC
            //: A3D_CAMERA2_F_I : UPDATED AT END OF FUNC
            
        //:----------------------------:Get_Point_On_Path://
////    :Get_Current_And_Next_Cams:------------------------:
        #define ARR A3D_CAMERA2_ARR
        #define DEX A3D_CAMERA2_DEX
        #define LEN A3D_CAMERA2_LEN
        
            CA2* c_0 = &( ARR[  DEX        ] );
            CA2* c_1 = &( ARR[ (DEX+1)%LEN ] );
            
            if( A3C_BUG >= 1 ){
            #define M_I ( A3M_M_I_SID_MOS )
            
                if( (*c_0).sid < 0 || (*c_0).sid > M_I ){
                    ERR("[C_0_SID_OOB]");
                };;
                if( (*c_1).sid < 0 || (*c_1).sid > M_I ){
                    ERR("[C_1_SID_OOB]");
                };;
                
            #undef  M_I
            };;
        
        #undef  ARR
        #undef  DEX
        #undef  LEN
////    :------------------------:Get_Current_And_Next_Cams:
////    :Interpolate:--------------------------------------:
        #define C_0 (*c_0)
        #define C_1 (*c_1)
        
            assert( A3D_CAMERA2_F_I <=
                ( C_0.f_0 + C_0.f_1 - 1 ) );;
                
            I32 lfi ; //:Local_Frame_Index
            I32 lft ; //:Local_Frame_Total
            I32 m_i ; //:maximum frame index 
            
            m_i =( C_0.f_0 + C_1.f_1 -1 );

            if( A3C_BUG >= 1 ){
                if( A3D_CAMERA2_F_I > m_i ){
                
                    ERR("[Interpolation_OOB]");
                };;
            };;
            
            if( A3D_CAMERA2_F_I < C_0.f_0 ){
            
                //:Stuck to target of c_0
                lfi=( 0 );
                lft=( 1 );
                
            }else
            if( A3D_CAMERA2_F_I >= C_0.f_0 ){
            
                //:Tween From c_0 to c_1
                lfi = ( A3D_CAMERA2_F_I - C_0.f_0 );
                lft = ( C_0.f_1 ); 
            };;
            
            a3f_camera2_mov( 
                C_0.sid  //:<-- can be same address
            ,   C_1.sid  //:<-- can be same address
            ,   lfi
            ,   lft
            );;

        #undef  C_0
        #undef  C_1
////    :--------------------------------------:Interpolate: 
        //:Render_Frame_And_Save_Image:------------------://
        
            A3F_CAMERA2_VP1_VP0(); //:Rende_Frame://
            A3F_CAMERA2_IMG_SAV(); //:Save__Image://
        
        //:------------------:Render_Frame_And_Save_Image://
        //:Calc_Next_Frame:------------------------------://
        
            a3d_camera2_img++;  //:Next Image Index To Save
            A3D_CAMERA2_F_I++;  //:Next PointToPoint @F_I@
            if( A3D_CAMERA2_F_I > m_i ){
            
                A3D_CAMERA2_F_I=( 0 );
                A3D_CAMERA2_DEX++;
                
                if( A3D_CAMERA2_DEX > (A3D_CAMERA2_LEN-1) ){
                
                    //:#_END_OF_THE_LINE_#://
                    a3d_camera2_kot=( 0 );
                    A3F_CAMERA2_KIL(    );
                };;
                
            };;
        //:------------------------------:Calc_Next_Frame://
        
    }
    
    VOD A3F_CAMERA2_RUN( VOD ){
    
        a3d_camera2_img =( 0 ); //:Image Index To Save
        A3D_CAMERA2_DEX =( 0 ); //:Camera KeyFrame?
        A3D_CAMERA2_F_I =( 0 ); //:FrameIndex between keys.
        a3d_camera2_kot =( 1 ); //:Keep On Ticking?
    }
    
//:========================================:CAMERA2_FUNCTIONS://
//:CAMERA_HIJACKING:=========================================://

    //:CAMERA2_SYSTEM_HIJACKING_FUNCTIONS:===================://

        VOD a3f_camera2_vp0_gpu( VOD ){    //:#_WHY_VP0_GPU_#://
        
            U32 x_0 =(U32)( 0 );
            U32 x_1 =(U32)( a3d_camera2_pix_wid - 1 );
            U32 y_0 =(U32)( 0 );
            U32 y_1 =(U32)( a3d_camera2_pix_hig - 1 );
        
            A3F_TAUMAST_PUT( A3M_TAU_VP0_X01 , 0
            | ( x_0 << A3S_VP0_X_0 )
            | ( x_1 << A3S_VP0_X_1 )
            );;
            A3F_TAUMAST_PUT( A3M_TAU_VP0_Y01 , 0
            | ( y_0 << A3S_VP0_Y_0 )
            | ( y_1 << A3S_VP0_Y_1 )
            );;
        }
        VOD a3f_camera2_vpc_gpu( VOD ){    //:#_WHY_VPC_GPU_#://
        
            U32 x_0 =(U32)( 0 );
            U32 x_1 =(U32)( a3d_camera2_pix_wid - 1 );
            U32 y_0 =(U32)( 0 );
            U32 y_1 =(U32)( a3d_camera2_pix_hig - 1 );
        
            A3F_TAUMAST_PUT( A3M_TAU_VPC_X01 , 0
            | ( x_0 << A3S_VPC_X_0 )
            | ( x_1 << A3S_VPC_X_1 )
            );;
            A3F_TAUMAST_PUT( A3M_TAU_VPC_Y01 , 0
            | ( y_0 << A3S_VPC_Y_0 )
            | ( y_1 << A3S_VPC_Y_1 )
            );;
        }
        
        VOD a3f_camera2_hijacks( VOD ){
        
        //- a3f_camera2_vp1_gpu : Doesn't exist because  ://
        //:                      [vp1]is persisted each  ://
        //;                       frame.                 ://
            a3f_camera2_vpc_gpu();
            a3f_camera2_vp0_gpu();
        }
            
        
    //:===================:CAMERA2_SYSTEM_HIJACKING_FUNCTIONS://

    
//:=========================================:CAMERA_HIJACKING://
//:INIT_SYSTEM:==============================================://

    VOD A3F_CAMERA2_INI( VOD ){
                                                          
        //:#_BY_DEFAULT_CAMERA_LOCKED_TO_MAIN_PLAYER_#://
        
        a3d_camera2_pix_wid = a3m_camera2_ini_wid;
        a3d_camera2_pix_hig = a3m_camera2_ini_hig;
        
        
        a3f_camera2_hijacks(); //:Setup Viewports
        a3f_camera2_pix_ini(); //:Create___Bitmap
    }
    
//:==============================================:INIT_SYSTEM://
//:STAND_ALONE_APPLICATION_FUNCTION:=========================://


    //:STAND_ALONE_APPLICATION_FUNCTION:=====================://
    
        //:When running as stand alone application.
        VOD A3F_CAMERA2_APP( VOD ){
        
            //:#_APP_MUST_RESET_LEN_BECAUSE_INI_SETS_IT_#://
            A3D_CAMERA2_LEN =( 0 );
        
            //:Add camera keyframes:
            I32 A =( 2 );               //: A == f_0 scalar. ://
            I32 B =( 3 );               //: B == f_1 scalar. ://
            #define POP a3f_camera2_pop
                    POP(   0  ,   1 *A  ,   1 *B  );
                    POP(   1  ,   2 *A  ,   2 *B  );
                    POP(   2  ,   3 *A  ,   3 *B  );
                    POP(   3  ,   4 *A  ,   4 *B  );
                    POP(   4  ,   5 *A  ,   5 *B  );
                    POP(   5  ,   6 *A  ,   6 *B  );
                    POP(   6  ,   7 *A  ,   7 *B  );
                    POP(   7  ,   8 *A  ,   8 *B  );
                    POP(   8  ,   9 *A  ,   9 *B  );
                    POP(   9  ,  10 *A  ,  10 *B  );
                    POP(  10  ,  11 *A  ,  11 *B  );
                    POP(  11  ,  12 *A  ,  12 *B  );
                    POP(  12  ,  13 *A  ,  13 *B  );
                    POP(  13  ,  14 *A  ,  14 *B  );
                    POP(  14  ,  15 *A  ,  15 *B  );
                    POP(  15  ,  16 *A  ,  16 *B  );
            #undef  POP
        
            A3F_DGENLEP_MEP(); //:#_CREATE_LEVELPACK_#://
            
            A3F_CPUMANA_SOF( 0 ); //:Soft_Reset_Of_Level
            
            A3F_CAMERA2_RUN();

            //:Loop until system decides to stop looping:
            
            while( A3F_CAMERA2_KOT() >= 1 ){
                   A3F_CAMERA2_TIK();
            };;     
        }
    //:=====================:STAND_ALONE_APPLICATION_FUNCTION://

//:=========================:STAND_ALONE_APPLICATION_FUNCTION://
//:CAMERA2_FILE_MACROS:======================================://

    #undef  CHR
    #undef  I08
    #undef  U08  
    #undef  U32  
    #undef  I32  
    #undef  F64  
    #undef  IV2  
    #undef  REC  
    #undef  CAM  
    #undef  MOS  
    #undef  L80  
    #undef  N2D  
    #undef  CA2  
    #undef  VOD  
    #undef  LOG  
    #undef  ERR  
    
//:======================================:CAMERA2_FILE_MACROS://
//:MONKEY_WRENCH_MACROS:=====================================://

    #define A3F_CAMERA2_SAV_IMG [@NBV@:A3F_CAMERA2_IMG_SAV]

//:MONKEY_WRENCH_MACROS:=====================================://
//:COMMENTS_ARE_READ_LAST_OR_NEVER:==========================://
/** DO_NOT_EXTRACT_COMMENTS_FROM_THIS_FILE ***************** ***

    LevelPack Data ( LEP / LP0 / LP1 )
    Has 2 un-used bits on each pixel. Let's collect
    those bits to make[ U32 ]variables to pack more 
    data. This data will be expensive to fetch because
    1 32 bit variable requires looking at 16 pixels.
    Optimized, we only have to look at 16 bytes.
    Because we only need to see the blue bytes of pixel.
    
    |<------------------[ 512  pixels ]------------------>|
    |<------------------[  32  U32(s) ]------------------>|
    +-------+-------+-------+.....+-------+-------+-------+ 
    |  16   |  16   |  16   |     |  16   |  16   |  16   | 
    +-------+-------+-------+.....+-------+-------+-------+ 
    |< U32 >|< U32 >|< U32 >|     |< U32 >|< U32 >|< U32 >|



    #_BY_DEFAULT_CAMERA_LOCKED_TO_MAIN_PLAYER_#
        //:----------------------------------------------://
        //: By default, hack the camera so that it is    ://
        //: always animating to sprite # A3C_SID.        ://
        //: In other words, always following player.     ://
        //: Not exactly though, because camera actually  ://
        //: tweens to the CAMERA SETTINGS OF THE LEVEL   ://
        //: THAT THE SPRITE CURRENTLY RESIDES IN.        ://
        //:----------------------------------------------://
        
    #_APP_MUST_RESET_LEN_BECAUSE_INI_SETS_IT_# 
        //:------------------------------------------------://
        //: Setting points on path will not work correctly ://
        //: unless this is reset to zero from whatever the ://
        //: *_INI() method did to it.                      ://
        //:------------------------------------------------://
        
    #_GET_BOTH_SPRITES_CAMERA_RECTANGLES_#
        //:-----------------------------------------------://
        //: Based On The Sprite ID, where should the      ://
        //: Camera Be? Can't just look up the player      ://
        //: position here, because camera rectangle is    ://
        //: actually based on the settings of the room    ://
        //: that they player is in. Could be a platformer ://
        //: camera or a zoomed out puzzel camera.         ://
        //:-----------------------------------------------://
        
    #_FUNC_NOTES_CAMERA2_REC_#
    
        /// ************************************************ ///
        /// Compute the camera rectangle associated with     ///
        /// the sprite of interest (sid) within the active   ///
        /// level pack. Camera is only going to be able to   ///
        /// focus on sprite in the active level pack.        ///
        /// I don't see a reason the camera should ever      ///
        /// be viewing a sprite in the in-active level pack. ///
        /// Also, don't think simulations like sprite        ///
        /// position update and collision will even be       ///
        /// running on the in-active level pack.             ///
        /// ************************************************ ///
        
    @FGLUED@: Frames That We Are Glued/Locked To Sprite
    @F_NEXT@: Frames That We Use To Transition to next sprite.
    @SID@   : The SpriteID of the sprite being tracked.
    
    @lfi@   : Local Frame Index  (Current Frame Index In Range)
    @lft@   : Local Frame Total  (Total   Frames      In Range)
    
    #lfi# && #lft#:--------------------------------------------:
    
        Used to linearlly interpolate.
        We pass both of these around instead of a 
        floating point percentage because I don't want
        any ambiguity as to what the value means.
        
        0/5  &  5/5
        
        Has more meaning than:
        
        0.0  &  1.0
        
        Because... Maybe I want to do this:
        
            1/5 == min
            5/5 == max
            
            Or this:
            
            0/5 == min
            4/5 == max
            
            Or this:
            
            0/5 == min
            5/5 == max
            
        Float percentages loose that pixel perfect level
        of communication in the code.
        
    :--------------------------------------------:#lfi# && #lft#
    
    @R_0_REC@ : 1st rectangle in our interpolation.
    @R_1_REC@ : 2nd rectangle in our interpolation.
    
    #_END_OF_THE_LINE_# :
    
        You are finished moving the camera from point to
        point on the path. There is nothing left to 
        interpolate. Out of camera keyframes.
        
   #_WHY_VP0_GPU_# : ----------------------------------------- :
   #_WHY_VPC_GPU_# : ----------------------------------------- :
   
        *_APP function is integrated with the entire game
        engine. But it doesn't have local copies of 
        viewport 0 (VP0) and viewport c (VPC).
        
        It just hijacks the system values.
        
        The reason for this is to:
        
        1. Reduce errors because of lack of memory coherency.
        2. Simplify the code.
        3. Assure that, if the code works as stand alone app,
           it can in hypothesis, work in larger game engine
           if we need to use it.
           
    : -----------------------------------------: #_WHY_VP0_GPU_#
    : -----------------------------------------: #_WHY_VPC_GPU_#
    
    @PANC@ : Pixel_Array_Never_Created
    @AINI@ : Already_INItialized
    
    @F_I@  : Frame Index
           : Describes progress travelling between two
           : cameras on path, or the amount of hold time
           : on the starting sprite ( c_0 ).
           : NOT a global index describing the ENTIRE
           : polyline path of camera keyframes.
           
    @SDWR@ : Shutting_Down_While_Running 
           : This is not a good thing. But not bad enough
           : to be a halting error. Even for error happy me.
           
    #_ABOUT_CAMERA2_VP1_VP0_#
    
        Note: 
            This files function [ CAMERA2_VP1_VP0 ]has
            exact same logic as [ CAMERAG_VP1_VP0 ].
    
        VP1_VP0 means ( VP1_cto_VP0 )
        VP1_VP0 means ( VP1 ==> VP0 )
        
        I try to name all of my functions in terms of
        data transformation. Because all programs really
        do is transform data from one form to another.
        That is what Mike Acton says in his talks.
    
        /// **************************************** ///
        /// Loop through all pixels of [PNG|GIF]     ///
        /// bitmap and treat them as fragcoords.     ///
        /// Pump fragcoords through render code      ///
        /// and save result to current bitmap.       ///
        /// **************************************** ///
        
    @NBV@ : Nouns Before Verbs (Its a rule)
    
    #_WHY_IMG_SAV_#
    
        1:  Because @NBV@ 
        
        2:
            Also, things should be named in transformation
            order. Camera data is the input and the save file
            for image is the output.
            
            IMG == input
            SAV == save file output
            
    @bas_fol@ : Base Folder
    @bas_nam@ : Base Name
    
    @PIX_ARR@ : PIXel ARRay
    @PIX_WID@ : PIXel WIDth  (width  in pixels)
    @PIX_HIG@ : PIXel HeIGht (height in pixels)
    
    #_FTL_DEF_MOS_POS_#
        ForgotToLoad__DEFault__MOvableSprite__POSitions?
        We probably need some type of level soft reset
        function.
        
** ****************** DO_NOT_EXTRACT_COMMENTS_FROM_THIS_FILE **/
//:==========================:COMMENTS_ARE_READ_LAST_OR_NEVER://