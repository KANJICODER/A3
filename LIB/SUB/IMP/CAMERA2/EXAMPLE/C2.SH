    ##  SC[ hkmf-mini] #########################################
    ## SEE[ hkmf-c11 ] for reference:  #########################
    ############################################################
    gcc                                                        \
        -x c                                                   \
        -c "C2.C11"                                            \
        -o obj.obj                                             \
                                                               \
            -Wconversion                                       \
                                                               \
            -Werror                                            \
            -Wfatal-errors                                     \
            -Wpedantic                                         \
            -Wall                                              \
            -Wextra                                            \
                                                               \
            -fstrict-aliasing                                  \
            -Wstrict-aliasing                                  \
                                                               \
            -std=c11                                           \
            -m64 ###############################################
                                    ####                    ####
    gcc -o exe.exe obj.obj          ####                    ####                
####rm             obj.obj          ####                    ####     
####     ./exe.exe                  ####                    ####
####rm     exe.exe                  ####                    ####
####                                ####                    ####
####read -p "[ENTER_TO_EXIT]:"      ####                    ####
    ############################################################

    ./exe.exe
    exit_code=$?
    
    echo ""
    echo ""
    if (( $exit_code == 11 )) ; then
    
        echo "[segmentation_fault(11)]" 
    fi
    if (( $exit_code ==  0 )) ; then
        
       echo  "[exit_is_okay]"
    fi
    if (( $exit_code != 0 )) ; then
        
       echo "[bad_exit]"
    fi
    
    echo   "[your_exit_code]: " $exit_code
    echo ""

    rm exe.exe
    rm obj.obj