
    #_WHY_IIT_AND_MEP_NOT_IN_A_FUNCTION_FILE_# :
    
        MEP() function is not in the function
        file for LIBMAIN because the main entry point (MEP)
        function needs to know about all the other LIBMAIN
        functions. And if we want to avoid forward declarations
        in our code, we must organize MEP() into[ L_M._ ]
        instead of [ LM.M._ ]. Well... If we want to easily
        see where it is anyhow.
        
        IIT() similiarily calls all of the *_INI & *_UTM
        functions for all sub systems. 
        INCLUDING THE *_INI & *_UTM FOR LIBMAIN!!!!!
        So it also, for sake of not burrying at bottom of
        [ LM.F._ ]has been put into this file( L_M._ )
        
    A3F_LIBMAIN_KOT :
    
        Function we execute to see if the main loop should
        "Keep On Ticking" (KOT).