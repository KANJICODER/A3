//:ABOUT_AA3_DOT_H(AA3.H):===================================://
//:                                                          ://
//:     My coding style is what some might call              ://
//:     "bat shit crazy". But I would really like other's    ://
//:     to use my code one day. I have created this          ://
//:     library struct to expose functions in a more         ://
//:     pallatable way this is more intellisense             ://
//:     friendly.                                            ://
//:                                                          ://
//:===================================:ABOUT_AA3_DOT_H(AA3.H)://
//:FILE_MACROS:==============================================://
#define VOD void     //:-------------------------------------://
#define U08 uint8_t  //:-------------------------------------://
#define I32 int32_t  //:-------------------------------------://
//:==============================================:FILE_MACROS://
//:MOLOTOV:==================================================://

    typedef
        VOD  ( A3T_MOLOTOV_ADD_MAK )( 

            U08*    pix_512  //:INPUT_IMAGE ://
        ,   I32     wid_512  //:INPUT_IMAGE ://
        ,   I32     hig_512  //:INPUT_IMAGE ://

        ,   U08* *r_out_pix  //:OUTPUT_IMAGE:// 
        ,   I32  *r_out_wid  //:OUTPUT_IMAGE://
        ,   I32  *r_out_hig  //:OUTPUT_IMAGE://

        );

    typedef
        VOD  ( A3T_MOLOTOV_ADD_DEL )( 

            U08* *r_out_pix  //:OUTPUT_IMAGE:// 
        ,   I32  *r_out_wid  //:OUTPUT_IMAGE://
        ,   I32  *r_out_hig  //:OUTPUT_IMAGE://

        );

    struct A3T_MOLOTOV_LIB{

        A3T_MOLOTOV_ADD_MAK*  SquareAddMake   ;
        A3T_MOLOTOV_ADD_DEL*  SquareAddDelete ;
        A3T_MOLOTOV_ADD_MAK*  CircleAddMake   ;
        A3T_MOLOTOV_ADD_DEL*  CircleAddDelete ;
    };

//:==================================================:MOLOTOV://
//:PUBLIC_LIBRARY_STRUCT_FOR_3RD_PARTIES:====================://

    #undef  A3M_PUBLIC_LIBRARY_STRUCT_INCLUDED
    #define A3M_PUBLIC_LIBRARY_STRUCT_INCLUDED ( 1 )

    struct aa3_lib{

        struct A3T_MOLOTOV_LIB MOLOTOV ;

    }AA3={ 0 };

//:====================:PUBLIC_LIBRARY_STRUCT_FOR_3RD_PARTIES://
//:FILE_MACROS:==============================================://
#undef  VOD void     //:-------------------------------------://
#undef  U08 uint8_t  //:-------------------------------------://
#undef  I32 int32_t  //:-------------------------------------://
//:==============================================:FILE_MACROS://
    